#!/bin/sh -e
#######################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################
 
if test "$LEAKS" = 1
then
    printf -- '\n---------------------------\n'
    printf -- 'Testing evaluator for leaks\n'
    printf -- '---------------------------\n\n'
else
    printf -- '\n-----------------\n'
    printf -- 'Testing evaluator\n'
    printf -- '-----------------\n\n'
fi

ENABLE_X11="$(./guish -b | grep -q '(Xlib): yes' && printf 1)"
ENABLE_CONTROL="$(./guish -b | grep -q '(Xtst): yes' && printf 1)"
ENABLE_IMAGES="$(./guish -b | grep -q '(Imlib2): yes' && printf 1)"

tfunc="$(./guish -b | grep -q 'Debug activated: yes' && printf egtest || printf etest)"
test "$LEAKS" = 1 && tfunc=ltest

. tests/utils.sh

$tfunc -c "add('')" "[<cmdline>] at '1': error, empty token instead of number!"
$tfunc -c "add(a)" "[<cmdline>] at '1': error, invalid number 'a'"
$tfunc -c "puts @" "[<cmdline>] at '1': error, bad dereference."
$tfunc -c "puts @" "[<cmdline>] at '1': error, bad dereference."
$tfunc -c "[]" "[<cmdline>] at '1': error, slice expression requires at least an index."
$tfunc -c "[{}]" "[<cmdline>] at '1': error, block given instead of an index."
$tfunc -c "[0]" "[<cmdline>] at '1': error, slice expression requires an object."
$tfunc -c "puts ''()" "[<cmdline>] at '1': error, '' is not a function!"
$tfunc -c "puts 5..4" "[<cmdline>] at '1': error, 'range' start number must be greater than end one."
$tfunc -c "puts div(7, 0)" "[<cmdline>] at '1': error, division by zero."
$tfunc -c "builtin(asdf)" "[<cmdline>] at '1': error, 'asdf' is not a builtin function!"
$tfunc -c "each(asdf)" "[<cmdline>] at '1': error, 'each' first argument must be a function!"
$tfunc -c "read(asdf)" "[<cmdline>] at '1': error, unable to read 'asdf'."
$tfunc -c "eq()" "[<cmdline>] at '1': error, wrong number of arguments for function 'eq'."
$tfunc -c "if" "[<cmdline>] at '1': error, wrong number of arguments for command 'if'."
$tfunc -c "{}" "[<cmdline>] at '1': error, unused block!"
$tfunc -c "''" "[<cmdline>] at '1': error, unused string ''!"

UD=a
while :
do
    if test -e "$UD"
    then UD="${UD}a"
    else break
    fi
done
egtest -c "cd $UD" "[<cmdline>] at '1': error, unable to change working dir."

$tfunc -c "while" "[<cmdline>] at '1': error, wrong number of arguments for command 'while'."
$tfunc -c "while 1" "[<cmdline>] at '1': error, wrong number of arguments for command 'while'."
$tfunc -c "for x" "[<cmdline>] at '1': error, wrong number of arguments for command 'for'."

if test "$ENABLE_X11"
then
    if test ! "$DISPLAY"
    then
        $tfunc -c "puts*" "[<cmdline>] at '1': error, no display!"
        $tfunc -c "puts|b|" "[<cmdline>] at '1': error, no display!"
        $tfunc -c "puts<(123)" "[<cmdline>] at '1': error, no display!"
        $tfunc -c "exists(x)" "[<cmdline>] at '1': error, no display!"
        $tfunc -c "=>g{}" "[<cmdline>] at '1': error, no display!"
        $tfunc -c "del 1" "[<cmdline>] at '1': error, no display!"
        $tfunc -c "ls" "[<cmdline>] at '1': error, no display!"
        $tfunc -c "asdf" "[<cmdline>] at '1': error, no display and/or wrong syntax!"

        if test "$ENABLE_CONTROL"
        then
            $tfunc -c "send a a" "[<cmdline>] at '1': error, no display!"
            $tfunc -c "ctrl a a" "[<cmdline>] at '1': error, no display!"
        fi
    else
        $tfunc -c "|b|;x .= 0" "[<cmdline>] at '1': error, cannot override default attributes!"
        $tfunc -c "|b|.asdf" "[<cmdline>] at '1': error, no such attribute 'asdf'."
        $tfunc -c "puts||" "[<cmdline>] at '1': error, empty element expression!"
        $tfunc -c "puts|g|" "[<cmdline>] at '1': error, unknown element 'g'."
        $tfunc -c "puts|123|" "[<cmdline>] at '1': error, no existing window with id '123'."
        $tfunc -c "puts<(ASDFasdf)" "[<cmdline>] at '1': error, cannot find a window id for 'ASDFasdf'."
        $tfunc -c "123" "[<cmdline>] at '1': error, unknown window id '123'."
        $tfunc -c "=>g{}" "[<cmdline>] at '1': error, no implied subject."
        $tfunc -c "|b|;=>g{}" "[<cmdline>] at '1': error, unknown signal 'g'."
        $tfunc -c "del 1" "[<cmdline>] at '1': warning, no window with id '1'."
        $tfunc -c "rel 1 2 l" "[<cmdline>] at '1': error, no widget with id '1'."
        $tfunc -c "rel |b| 2 l" "[<cmdline>] at '1': error, no widget with id '2'."
        
        if test "$ENABLE_CONTROL"
        then
            $tfunc -c "send 1 x" "[<cmdline>] at '1': error, no widget with id '1'."
            $tfunc -c "ctrl 1 Return" "[<cmdline>] at '1': error, no widget with id '1'."
        fi
    fi
else
    $tfunc -c "asdf" "[<cmdline>] at '1': error, unknown command 'asdf'."
fi

printf '\n'
