/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef INPUT_H
#define INPUT_H

#include "../widget.h"

typedef struct input_t {
    widget_t w;
    int itype;
    size_t cpos;
    void (*free)(struct input_t*);
} input_t;

input_t* input_t_init(input_t*, int, int);
void input_t_free(input_t*);

void buildtext(widget_t*, XEvent*);
void return_pressed(widget_t*);
void hideinput(widget_t*);
void password(widget_t*);
void showinput(widget_t*);
void tret(widget_t*);

#endif
