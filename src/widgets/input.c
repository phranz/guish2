/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include "input.h"
#include "../syntax.h"
#include "../dectypes.h"
#include "../evaluator.h"

extern GC gc;
extern Atom delatom;
extern Display* display;
extern Window root;
extern int screen;
extern map_strcol_t* colors; 

extern map_widloci_t* widlines;
extern map_widloci_t* widareas;
extern map_widarcssets_t* widarcssets;
extern map_widarcssets_t* widarcsareas;
extern map_widloci_t* widloci;
extern map_widloci_t* widpixels;
extern map_widnamedpoints_t* widnamedpoints;

enum inputtype {
    IN_SHOW,
    IN_OBSCURE,
    IN_HIDE
};

static long mflags;
#define I_CP 1
#define I_AP (1 << 1)
#define I_MP (1 << 2)

void buildtext(widget_t* w, XEvent* e) {
    input_t* p = (input_t*)w;
    char c;
    char C[2] = {0};
    size_t l = strlen(w->data);
    KeySym s;
    XLookupString(&e->xkey, &c, 1, &s, NULL);
    C[0] = c;

    switch (s) {
        case XK_BackSpace:
            if (e->type == KeyPress && p->cpos) {
                --p->cpos;
                for (size_t i=p->cpos; i<l; ++i)
                    w->data[i] = w->data[i+1];
            } 
            break;
        case XK_Return:
            if (e->type == KeyPress) {
                return_pressed(w);
                if (w->flags & F_NLONRETURN) {
                    C[0] = '\n';
                    w->data = sins(w->data, C, p->cpos);
                    ++p->cpos;
                }
            }
            break;
        case XK_Left:
            if (e->type == KeyPress && p->cpos)
                --p->cpos;
            break;
        case XK_Right:
            if (e->type == KeyPress && p->cpos < l)
                ++p->cpos;
            break;
        case XK_Up: {
            if (e->type != KeyPress || w->ll->count <= 1)
                break;

            size_t nl = 0;
            size_t pl = 0;
            int ismin = 1;
            char* ls;

            for (size_t i=(p->cpos ? p->cpos-1 : p->cpos); i; --i)
                if (w->data[i] == '\n')
                    ++nl;
            if (!nl)
                break;

            w->ll->get(w->ll, nl-1, &ls);
            pl = strlen(ls);

            for (size_t i=p->cpos-pl; i && i<p->cpos; ++i) {
                if (w->data[i] == '\n') {
                    ismin = 0;
                    break;
                }
            }

            if (!ismin) {
                p->cpos -= pl + 1;
                break;
            }
            p->cpos = 0;
            for (size_t i=0; i<nl; ++i) {
                w->ll->get(w->ll, i, &ls);
                p->cpos += strlen(ls) + 1;
            }
            --p->cpos;
            break;
        }
        case XK_Down: {
            if (e->type != KeyPress || w->ll->count <= 1)
                break;

            size_t nl = 0;
            size_t pl = 0;
            int f = 0;
            char* ls;

            for (size_t i=(p->cpos ? p->cpos-1 : p->cpos); i; --i)
                if (w->data[i] == '\n')
                    ++nl;

            if (nl == w->ll->count-1)
                break;

            if (!w->ll->get(w->ll, nl, &ls))
                break;

            pl = strlen(ls) + 1;

            for (size_t i=p->cpos; i<p->cpos+pl; ++i)
                if (!w->data[i] || w->data[i] == '\n')
                    ++f;

            if (f == 1) {
                p->cpos += pl;
                break;
            }
            if (w->ll->get(w->ll, nl+1, &ls)) {
                p->cpos = 0;
                p->cpos += strlen(ls);
                for (size_t i=0; i<=nl; ++i) {
                    w->ll->get(w->ll, i, &ls);
                    p->cpos += strlen(ls) + 1;
                }
            }
            break;
        }

        case XK_Home:
            if (e->type == KeyPress)
                p->cpos = 0;
            break;
        case XK_End:
            if (e->type == KeyPress)
                p->cpos = l;
            break;
        case XK_Control_L:
        case XK_Control_R:
            mflags = e->type == KeyRelease ? mflags & ~I_CP : mflags | I_CP;
            break;
        case XK_Alt_L:
        case XK_Alt_R:
            mflags = e->type == KeyRelease ? mflags & ~I_AP : mflags | I_AP;
            break;
        default:
            if (!(mflags & I_CP) && !(mflags & I_AP) && e->type == KeyPress && s >= 0x20 && s <= 0x7e) {
                C[0] = s;
                w->data = sins(w->data, C, p->cpos++);
            }
    }
}

static void draw(widget_t* w) {
    input_t* p = (input_t*)w;

    if (w->flags & F_HOVERED && w->flags & F_APPLYHOVER) {
        XSetWindowBackground(display, w->wid, w->hbg->pixel);
        XSetForeground(display, gc, w->hfg->pixel);
    #ifdef ENABLE_IMAGES
    } else if (w->img) {
        updateimg(w);
    #endif
    } else {
        XSetWindowBackground(display, w->wid, w->bg->pixel);
        XSetForeground(display, gc, w->fg->pixel);
    }
    XSetFont(display, gc, w->fs->fid);
    XClearWindow(display, w->wid);
    if (p->itype == IN_HIDE)
        return;

    intint_t c;
    if (!*w->data) {
        c = textpos(w, "", w->align, 0, 1);
        XDrawLine(display, w->wid, gc, c.key, c.val, c.key, c.val-w->fh);
        return;
    }
    if (p->itype == IN_OBSCURE) {
        char* x = NULL;
        for (size_t i=0; i<strlen(w->data); ++i)
            x = sadd(x, "*");
        write_align(w, x, w->align, &c);
        free(x);
    } else {
        write_align(w, w->data, w->align, &c);
    }
    if (c.key < 0)
        return;

    size_t l = 0;
    size_t nl = 0;
    char* ls;

    for (size_t i=(p->cpos ? p->cpos-1 : p->cpos); i; --i)
        if (w->data[i] == '\n')
            ++nl;

    for (size_t i=0; i<nl; ++i) {
        w->ll->get(w->ll, i, &ls);
        l += strlen(ls) + 1;
    }
    w->ll->get(w->ll, nl, &ls);

    c.key = XTextWidth(w->fs, ls, p->cpos-l) + w->m;
    c.val = c.val - (((w->ll->count-1) == nl) ? 0 : ((w->ll->count-1 - nl) * w->fh));
    XDrawLine(display, w->wid, gc, c.key, c.val, c.key, c.val-w->fh);
    if (widareas)
        drawareas(w);
    if (widarcsareas)
        drawarcsareas(w);
    if (widlines)
        drawlines(w);
    if (widarcssets)
        drawarcs(w);
    if (widloci)
        drawpoints(w);
    if (widpixels)
        drawpixels(w);
    if (widnamedpoints)
        drawnamedpoints(w);
}

void tret(widget_t* w) {
    w->flags = (w->flags & F_NLONRETURN) ? w->flags & ~F_NLONRETURN : w->flags | F_NLONRETURN;
}

void return_pressed(widget_t* w) {
    trigger(w->wid, SIG_RETURNPRESSED);
}

void hideinput(widget_t* w) {
    input_t* p = (input_t*)w;
    p->itype = IN_HIDE;
    w->draw(w);
}

void password(widget_t* w) {
    input_t* p = (input_t*)w;
    p->itype = IN_OBSCURE;
    w->draw(w);
}

void showinput(widget_t* w) {
    input_t* p = (input_t*)w;

    p->itype = IN_SHOW;
    w->draw(w);
}

void input_t_free(input_t* p) {
    ((widget_t*)p)->free((widget_t*)p);
    free(p);
}

input_t* input_t_init(input_t* p, int ww, int hh) {
    if (!p)
        return NULL;
    memset(p, 0, sizeof(input_t));

    widget_t* w = (widget_t*)p;
    widget_t_init(w);

    p->itype = IN_SHOW;

    p->free = input_t_free;
    w->draw = draw;

    w->w = ww ? ww : 120;
    w->h = hh ? hh : 30;
    w->b = 2;

    colors->get(colors, "white", &w->bg);
    colors->get(colors, "black", &w->fg);

    colors->get(colors, "white", &w->pbg);
    colors->get(colors, "black", &w->pfg);

    colors->get(colors, "white", &w->hbg);
    colors->get(colors, "black", &w->hfg);

    XSetWindowAttributes wa = {0};
    w->wid = XCreateWindow(display, root, w->x, w->y, w->w, w->h, w->b, DefaultDepth(display, screen), InputOutput, DefaultVisual(display, screen), CWOverrideRedirect, &wa);

    w->mask = VisibilityChangeMask|ExposureMask|KeyPressMask|KeyReleaseMask|StructureNotifyMask;
    XSelectInput(display, w->wid, w->mask);

    XSetWMProtocols(display, w->wid, &delatom, 1);
    fixed(w, 0);
    w->align = A_ML;

    return p;
}
