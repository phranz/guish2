/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>

#include "dectypes.h"
#include "syntax.h"

#ifdef ENABLE_X11
typedef struct {
    map_strint_t* commands;
    map_strint_t* sigs;
} elementdata_t;

static char* elements[E_END];
static char* attrs[ATTR_END];
static elementdata_t composition[E_END];
static map_strint_t* eids;
static map_strint_t* cids;
static map_strint_t* aids;

int sid(int e, char* g) {
    if (!e)
        return SIG_NONE;

    int s;
    if (composition[e].sigs->get(composition[e].sigs, g, &s))
        return s;
    return SIG_NONE;
}

int eid(const char* n) {
    if (!n || !*n)
        return E_ALL;

    for (int i=E_ALL+1; i<E_END; ++i) {
        if (!elements[i])
            continue;
        if (eq(elements[i], n))
            return i;
    }
    return E_NONE;
}

int cidbe(int e, char* s) {
    if (!composition[e].commands)
        return CMD_NONE;

    int c;
    if (composition[e].commands->get(composition[e].commands, s, &c))
        return c;
    return CMD_NONE;
}

int cid(const char* n) {
    if (!n || !*n)
        return CMD_NONE;
    int c;
    if (cids->get(cids, (char*)n, &c))
        return c;

    return CMD_NONE;
}

int aid(const char* n) {
    if (!n || !*n)
        return ATTR_NONE;
    int a;
    if (aids->get(aids, (char*)n, &a))
        return a;

    return ATTR_NONE;
}

const char* ename(int e) {
    if (!e || e == E_ALL || !elements[e])
        return NULL;

    return elements[e];
}

int sidbe(int e, char* t) {
    if (!composition[E_ALL].sigs || !t)
        return SIG_NONE;

    int g;
    if (composition[E_ALL].sigs->get(composition[E_ALL].sigs, t, &g))
        return g;

    if (!composition[e].sigs)
        return SIG_NONE;

    if (composition[e].sigs->get(composition[e].sigs, t, &g))
        return g;

    return SIG_NONE;
}
#endif

char* commands[CMD_END];
static char* funcs[F_END];
static char* sigs[SIG_END];
static int cmdargs[CMD_END];
static int funcargs[F_END];

static map_strint_t* kids;

static map_strint_t* sids; // used just for general signals
static map_strint_t* fids;

int gsid(char* s) {
    int g;
    if (sids->get(sids, s, &g))
        return g;
    return SIG_NONE;
}

int kid(const char* n) {
    if (!n || !*n)
        return CMD_NONE;
    int k;
    if (kids->get(kids, (char*)n, &k))
        return k;

    return CMD_NONE;
}

int fid(const char* n) {
    if (!n || !*n)
        return F_NONE;
    int f;
    if (fids->get(fids, (char*)n, &f))
        return f;
    return F_NONE;
}

int cargs(int c) {
    return cmdargs[c];
}

int fargs(int c) {
    return funcargs[c];
}

const char* fname(int e) {
    if (!e || e == F_END)
        return NULL;

    return funcs[e];
}

void syntax_free() {
    #ifdef ENABLE_X11
    for (int i=E_ALL; i<E_END; ++i) {
        release(composition[i].commands);
        release(composition[i].sigs);
    }
    release(eids);
    release(cids);
    release(aids);
    #endif
    release(kids);
    release(sids);
    release(fids);
}

void syntax_init() {
    /* [NOTE FOR FUNCS AND COMMANDS] number of arguments:
     *  0 = none
     * -1 = variable
     * -X = at least (X-1) args, and variable
     *  X = there must be X args
     */

    int e = 0;

    #ifdef ENABLE_X11
    // elements
    elements[E_BUTTON] = "b";
    elements[E_INPUT] = "i";
    elements[E_PAGE] = "p";
    elements[E_LABEL] = "l";
    elements[E_CHECKBOX] = "c";
    elements[E_TRANS] = "t";

    // funcs
    funcs[F_EXISTS] = "exists";       funcargs[F_EXISTS] = 1;

    // attrs
    attrs[ATTR_W] = "w";
    attrs[ATTR_H] = "h";
    attrs[ATTR_X] = "x";
    attrs[ATTR_Y] = "y";
    attrs[ATTR_B] = "b";
    attrs[ATTR_G] = "g";
    attrs[ATTR_D] = "d";
    attrs[ATTR_C] = "c";
    attrs[ATTR_V] = "v";
    attrs[ATTR_E] = "e";
    attrs[ATTR_F] = "f";
    attrs[ATTR_S] = "s";
    attrs[ATTR_N] = "n";
    attrs[ATTR_TL] = "T";
    attrs[ATTR_T] = "t";
    attrs[ATTR_PID] = "pid";

    aids = alloc(map_strint_t);
    aids->strcmp = 1;

    for (e=ATTR_NONE+1; e<ATTR_END; ++e)
        aids->insert(aids, attrs[e], e);

    // commands
    commands[CMD_DELETE] = "del";                  cmdargs[CMD_DELETE] = 1;
    commands[CMD_RELATE] = "rel";                  cmdargs[CMD_RELATE] = 3;
    commands[CMD_WIDS] = "ls";                     cmdargs[CMD_WIDS] = -1;
    #ifdef ENABLE_CONTROL
    commands[CMD_SENDKEYS] = "send";               cmdargs[CMD_SENDKEYS] = 2;
    commands[CMD_SENDCONTROLKEYS] = "ctrl";        cmdargs[CMD_SENDCONTROLKEYS] = 2;
    #endif
    // common
    commands[CMD_TRAY] = "T";                      cmdargs[CMD_TRAY] = 0;
    commands[CMD_GHOST] = "G";                     cmdargs[CMD_GHOST] = 0;
    commands[CMD_FULLSCREEN] = "F";                cmdargs[CMD_FULLSCREEN] = 0;
    commands[CMD_SETDEFAULT] = "d";                cmdargs[CMD_SETDEFAULT] = 0;
    commands[CMD_BYPASSWM] = "!";                  cmdargs[CMD_BYPASSWM] = 0;
    commands[CMD_FOLLOWWM] = "!!";                 cmdargs[CMD_FOLLOWWM] = 0;
    commands[CMD_CENTER] = "n";                    cmdargs[CMD_CENTER] = 0;
    commands[CMD_HIDE] = "-";                      cmdargs[CMD_HIDE] = 0;
    commands[CMD_SHOW] = "+";                      cmdargs[CMD_SHOW] = 0;
    commands[CMD_DRAW] = "/";                      cmdargs[CMD_DRAW] = -1;
    commands[CMD_FOCUS] = "f";                     cmdargs[CMD_FOCUS] = 0;
    commands[CMD_TOP] = "t";                       cmdargs[CMD_TOP] = 0;
    commands[CMD_BOTTOM] = "b";                    cmdargs[CMD_BOTTOM] = 0;
    commands[CMD_CLOSE] = "x";                     cmdargs[CMD_CLOSE] = 0;
    commands[CMD_CLICK] = "c";                     cmdargs[CMD_CLICK] = 0;
    commands[CMD_LOWER] = "l";                     cmdargs[CMD_LOWER] = 0;
    commands[CMD_RAISE] = "r";                     cmdargs[CMD_RAISE] = 0;
    commands[CMD_MAXIMIZE] = "M";                  cmdargs[CMD_MAXIMIZE] = 0;
    commands[CMD_FREEZE] = "D";                    cmdargs[CMD_FREEZE] = 0;
    commands[CMD_UNFREEZE] = "E";                  cmdargs[CMD_UNFREEZE] = 0;
    commands[CMD_FIT] = "o";                       cmdargs[CMD_FIT] = 0;
    commands[CMD_FILL] = "w";                      cmdargs[CMD_FILL] = 0;
    commands[CMD_FILLNOLIMIT] = "nfill";           cmdargs[CMD_FILLNOLIMIT] = 0;
    commands[CMD_FILLRIGHT] = "rfill";             cmdargs[CMD_FILLRIGHT] = 0;
    commands[CMD_FILLRIGHTNOLIMIT] = "nrfill";     cmdargs[CMD_FILLRIGHTNOLIMIT] = 0;
    commands[CMD_FILLBOTTOM] = "bfill";            cmdargs[CMD_FILLBOTTOM] = 0;
    commands[CMD_FILLBOTTOMNOLIMIT] = "nbfill";    cmdargs[CMD_FILLBOTTOMNOLIMIT] = 0;
    commands[CMD_CLEAR] = "L";                     cmdargs[CMD_CLEAR] = 0;
    commands[CMD_ENTITLE] = ":";                   cmdargs[CMD_ENTITLE] = 1;
    commands[CMD_STYLE] = "s";                     cmdargs[CMD_STYLE] = 1;
    commands[CMD_RESIZE] = "z";                    cmdargs[CMD_RESIZE] = 2;
    commands[CMD_MOVE] = "m";                      cmdargs[CMD_MOVE] = 2;
    commands[CMD_TXMOVABLE] = "X";                 cmdargs[CMD_TXMOVABLE] = 0;
    commands[CMD_TYMOVABLE] = "Y";                 cmdargs[CMD_TYMOVABLE] = 0;
    commands[CMD_MOVEALIGN] = "A";                 cmdargs[CMD_MOVEALIGN] = 2;
    // common to some elements
    commands[CMD_SETTEXT] = "<";                   cmdargs[CMD_SETTEXT] = 1;
    commands[CMD_ADDTEXT] = "<+";                  cmdargs[CMD_ADDTEXT] = 1;
    commands[CMD_DEFTEXT] = ">";                   cmdargs[CMD_DEFTEXT] = 1;
    commands[CMD_TGRIP] = "g";                     cmdargs[CMD_TGRIP] = 0;
    // checkbox
    commands[CMD_CHECK] = "C";                     cmdargs[CMD_CHECK] = 0;
    commands[CMD_UNCHECK] = "U";                   cmdargs[CMD_UNCHECK] = 0;
    // input
    commands[CMD_SHOWINPUT] = "S";                 cmdargs[CMD_SHOWINPUT] = 0;
    commands[CMD_HIDEINPUT] = "H";                 cmdargs[CMD_HIDEINPUT] = 0;
    commands[CMD_PASSWORD] = "P";                  cmdargs[CMD_PASSWORD] = 0;
    commands[CMD_TRET] = "W";                      cmdargs[CMD_TRET] = 0;
    // page
    commands[CMD_EQUALIZE] = "Q";                  cmdargs[CMD_EQUALIZE] = 0;
    commands[CMD_STYLESUBS] = "S";                 cmdargs[CMD_STYLESUBS] = 1;
    commands[CMD_EXPLODE] = "P";                   cmdargs[CMD_EXPLODE] = 0;
    commands[CMD_EMBED] = "<<";                    cmdargs[CMD_EMBED] = 1;
    commands[CMD_FITEMBED] = "<<<";                cmdargs[CMD_FITEMBED] = 1;
    commands[CMD_UNTIE] = ">>";                    cmdargs[CMD_UNTIE] = 1;
    commands[CMD_FITUNTIE] = ">>>";                cmdargs[CMD_FITUNTIE] = 1;
    commands[CMD_VERTICAL] = "v";                  cmdargs[CMD_VERTICAL] = 0;
    commands[CMD_HORIZONTAL] = "h";                cmdargs[CMD_HORIZONTAL] = 0;
    commands[CMD_SWAP] = "Z";                      cmdargs[CMD_SWAP] = 2;
    commands[CMD_INVERT] = "N";                    cmdargs[CMD_INVERT] = 0;

    // normal signals
    sigs[SIG_ENTER] = "e";
    sigs[SIG_LEAVE] = "l";
    sigs[SIG_FOCUS] = "f";
    sigs[SIG_UNFOCUS] = "u";
    sigs[SIG_CLOSED] = "x";
    sigs[SIG_CLICKED] = "c";
    sigs[SIG_LCLICKED] = "lc";
    sigs[SIG_RCLICKED] = "rc";
    sigs[SIG_MCLICKED] = "mc";
    sigs[SIG_DCLICKED] = "cc";
    sigs[SIG_LDCLICKED] = "lcc";
    sigs[SIG_RDCLICKED] = "rcc";
    sigs[SIG_MDCLICKED] = "mcc";
    sigs[SIG_PRESSED] = "p";
    sigs[SIG_LPRESSED] = "lp";
    sigs[SIG_RPRESSED] = "rp";
    sigs[SIG_MPRESSED] = "mp";
    sigs[SIG_RELEASED] = "r";
    sigs[SIG_LRELEASED] = "lr";
    sigs[SIG_RRELEASED] = "rr";
    sigs[SIG_MRELEASED] = "mr";
    sigs[SIG_SCROLLUP] = "S";
    sigs[SIG_SCROLLDOWN] = "s";
    sigs[SIG_MOVED] = "m";
    sigs[SIG_RESIZED] = "z";
    sigs[SIG_UNCHECKED] = "U";
    sigs[SIG_CHECKED] = "C";
    sigs[SIG_RETURNPRESSED] = "R";

    eids = alloc(map_strint_t);
    cids = alloc(map_strint_t);

    eids->strcmp = 1;
    cids->strcmp = 1;

    // elements
    for (e=EIDS_START; e<EIDS_END; ++e)
        eids->insert(eids, elements[e], e);

    // common commands
    for (e=CIDS_START; e<CIDS_END; ++e)
        cids->insert(cids, commands[e], e);

    if (cids->full(cids))
        cids->rehash(cids);

    // composition
    map_strint_t* cs = alloc(map_strint_t);
    map_strint_t* ss = alloc(map_strint_t);
    cs->strcmp = 1;
    ss->strcmp = 1;

    cs->insert(cs, commands[CMD_TRAY], CMD_TRAY);
    cs->insert(cs, commands[CMD_GHOST], CMD_GHOST);
    cs->insert(cs, commands[CMD_CENTER], CMD_CENTER);
    cs->insert(cs, commands[CMD_HIDE], CMD_HIDE);
    cs->insert(cs, commands[CMD_SHOW], CMD_SHOW);
    cs->insert(cs, commands[CMD_DRAW], CMD_DRAW);
    cs->insert(cs, commands[CMD_FOCUS], CMD_FOCUS);
    cs->insert(cs, commands[CMD_TOP], CMD_TOP);
    cs->insert(cs, commands[CMD_BOTTOM], CMD_BOTTOM);
    cs->insert(cs, commands[CMD_CLOSE], CMD_CLOSE);
    cs->insert(cs, commands[CMD_CLICK], CMD_CLICK);
    cs->insert(cs, commands[CMD_DELETE], CMD_DELETE);
    cs->insert(cs, commands[CMD_LOWER], CMD_LOWER);
    cs->insert(cs, commands[CMD_RAISE], CMD_RAISE);
    cs->insert(cs, commands[CMD_MAXIMIZE], CMD_MAXIMIZE);
    cs->insert(cs, commands[CMD_FREEZE], CMD_FREEZE);
    cs->insert(cs, commands[CMD_UNFREEZE], CMD_UNFREEZE);
    cs->insert(cs, commands[CMD_FIT], CMD_FIT);
    cs->insert(cs, commands[CMD_FILL], CMD_FILL);
    cs->insert(cs, commands[CMD_FILLNOLIMIT], CMD_FILLNOLIMIT);
    cs->insert(cs, commands[CMD_FILLRIGHT], CMD_FILLRIGHT);
    cs->insert(cs, commands[CMD_FILLRIGHTNOLIMIT], CMD_FILLRIGHTNOLIMIT);
    cs->insert(cs, commands[CMD_FILLBOTTOM], CMD_FILLBOTTOM);
    cs->insert(cs, commands[CMD_FILLBOTTOMNOLIMIT], CMD_FILLBOTTOMNOLIMIT);
    cs->insert(cs, commands[CMD_CLEAR], CMD_CLEAR);
    cs->insert(cs, commands[CMD_ENTITLE], CMD_ENTITLE);
    cs->insert(cs, commands[CMD_STYLE], CMD_STYLE);
    cs->insert(cs, commands[CMD_RESIZE], CMD_RESIZE);
    cs->insert(cs, commands[CMD_MOVE], CMD_MOVE);
    cs->insert(cs, commands[CMD_TXMOVABLE], CMD_TXMOVABLE);
    cs->insert(cs, commands[CMD_TYMOVABLE], CMD_TYMOVABLE);
    cs->insert(cs, commands[CMD_SETDEFAULT], CMD_SETDEFAULT);
    cs->insert(cs, commands[CMD_BYPASSWM], CMD_BYPASSWM);
    cs->insert(cs, commands[CMD_FOLLOWWM], CMD_FOLLOWWM);
    cs->insert(cs, commands[CMD_FULLSCREEN], CMD_FULLSCREEN);
    cs->insert(cs, commands[CMD_SETTEXT], CMD_SETTEXT);
    cs->insert(cs, commands[CMD_ADDTEXT], CMD_ADDTEXT);
    cs->insert(cs, commands[CMD_DEFTEXT], CMD_DEFTEXT);

    ss->insert(ss, sigs[SIG_ENTER], SIG_ENTER);
    ss->insert(ss, sigs[SIG_LEAVE], SIG_LEAVE);
    ss->insert(ss, sigs[SIG_FOCUS], SIG_FOCUS);
    ss->insert(ss, sigs[SIG_UNFOCUS], SIG_UNFOCUS);
    ss->insert(ss, sigs[SIG_CLOSED], SIG_CLOSED);
    ss->insert(ss, sigs[SIG_CLICKED], SIG_CLICKED);
    ss->insert(ss, sigs[SIG_LCLICKED], SIG_LCLICKED);
    ss->insert(ss, sigs[SIG_RCLICKED], SIG_RCLICKED);
    ss->insert(ss, sigs[SIG_MCLICKED], SIG_MCLICKED);
    ss->insert(ss, sigs[SIG_DCLICKED], SIG_DCLICKED);
    ss->insert(ss, sigs[SIG_LDCLICKED], SIG_LDCLICKED);
    ss->insert(ss, sigs[SIG_RDCLICKED], SIG_RDCLICKED);
    ss->insert(ss, sigs[SIG_MDCLICKED], SIG_MDCLICKED);
    ss->insert(ss, sigs[SIG_PRESSED], SIG_PRESSED);
    ss->insert(ss, sigs[SIG_LPRESSED], SIG_LPRESSED);
    ss->insert(ss, sigs[SIG_RPRESSED], SIG_RPRESSED);
    ss->insert(ss, sigs[SIG_MPRESSED], SIG_MPRESSED);
    ss->insert(ss, sigs[SIG_RELEASED], SIG_RELEASED);
    ss->insert(ss, sigs[SIG_LRELEASED], SIG_LRELEASED);
    ss->insert(ss, sigs[SIG_RRELEASED], SIG_RRELEASED);
    ss->insert(ss, sigs[SIG_MRELEASED], SIG_MRELEASED);
    ss->insert(ss, sigs[SIG_SCROLLUP], SIG_SCROLLUP);
    ss->insert(ss, sigs[SIG_SCROLLDOWN], SIG_SCROLLDOWN);
    ss->insert(ss, sigs[SIG_MOVED], SIG_MOVED);
    ss->insert(ss, sigs[SIG_RESIZED], SIG_RESIZED);

    if (cs->full(cs))
        cs->rehash(cs);
    if (ss->full(ss))
        ss->rehash(ss);

    composition[E_ALL].commands = cs;
    composition[E_ALL].sigs = ss;

    // button
    composition[E_BUTTON].commands = NULL;
    composition[E_BUTTON].sigs = NULL;

    // input
    cs = alloc(map_strint_t);
    ss = alloc(map_strint_t);
    cs->strcmp = 1;
    ss->strcmp = 1;

    cs->insert(cs, commands[CMD_SHOWINPUT], CMD_SHOWINPUT);
    cs->insert(cs, commands[CMD_HIDEINPUT], CMD_HIDEINPUT);
    cs->insert(cs, commands[CMD_PASSWORD], CMD_PASSWORD);
    cs->insert(cs, commands[CMD_TRET], CMD_TRET);
    ss->insert(ss, sigs[SIG_RETURNPRESSED], SIG_RETURNPRESSED);

    composition[E_INPUT].commands = cs;
    composition[E_INPUT].sigs = ss;

    // page
    cs = alloc(map_strint_t);
    ss = alloc(map_strint_t);
    cs->strcmp = 1;
    ss->strcmp = 1;

    cs->insert(cs, commands[CMD_EMBED], CMD_EMBED);
    cs->insert(cs, commands[CMD_FITEMBED], CMD_FITEMBED);
    cs->insert(cs, commands[CMD_UNTIE], CMD_UNTIE);
    cs->insert(cs, commands[CMD_FITUNTIE], CMD_FITUNTIE);
    cs->insert(cs, commands[CMD_EXPLODE], CMD_EXPLODE);
    cs->insert(cs, commands[CMD_VERTICAL], CMD_VERTICAL);
    cs->insert(cs, commands[CMD_HORIZONTAL], CMD_HORIZONTAL);
    cs->insert(cs, commands[CMD_SWAP], CMD_SWAP);
    cs->insert(cs, commands[CMD_INVERT], CMD_INVERT);
    cs->insert(cs, commands[CMD_EQUALIZE], CMD_EQUALIZE);
    cs->insert(cs, commands[CMD_STYLESUBS], CMD_STYLESUBS);

    composition[E_PAGE].commands = cs;
    composition[E_PAGE].sigs = ss;

    // label
    composition[E_LABEL].commands = NULL;
    composition[E_LABEL].sigs = NULL;

    // checkbox
    cs = alloc(map_strint_t);
    ss = alloc(map_strint_t);
    cs->strcmp = 1;
    ss->strcmp = 1;

    cs->insert(cs, commands[CMD_CHECK], CMD_CHECK);
    cs->insert(cs, commands[CMD_UNCHECK], CMD_UNCHECK);
    ss->insert(ss, sigs[SIG_CHECKED], SIG_CHECKED);
    ss->insert(ss, sigs[SIG_UNCHECKED], SIG_UNCHECKED);

    composition[E_CHECKBOX].commands = cs;
    composition[E_CHECKBOX].sigs = ss;
    #endif

    // functions
    funcs[F_READ] = "read";              funcargs[F_READ] = -1;
    funcs[F_WRITE] = "write";            funcargs[F_WRITE] = 2;
    funcs[F_APPEND] = "append";          funcargs[F_APPEND] = 2;
    funcs[F_EVAL] = "eval";              funcargs[F_EVAL] = -1;
    funcs[F_BUILTIN] = "builtin";        funcargs[F_BUILTIN] = -2;
    funcs[F_BLOCK] = "block";            funcargs[F_BLOCK] = -1;
    funcs[F_FLAT] = "flat";              funcargs[F_FLAT] = -1;
    funcs[F_EACH] = "each";              funcargs[F_EACH] = -2;
    funcs[F_LET] = "let";                funcargs[F_LET] = -1;
    funcs[F_SOME] = "some";              funcargs[F_SOME] = -1;
    funcs[F_TRUE] = "true";              funcargs[F_TRUE] = 1;
    funcs[F_FALSE] = "false";            funcargs[F_FALSE] = 1;
    funcs[F_ENV] = "env";                funcargs[F_ENV] = 1;
    funcs[F_CWD] = "cwd";                funcargs[F_CWD] = 0;
    funcs[F_PUTS] = "puts";              funcargs[F_PUTS] = -1;
    funcs[F_PUSH] = "push";              funcargs[F_PUSH] = -1;
    funcs[F_PUSHB] = "pushb";            funcargs[F_PUSHB] = -1;
    funcs[F_POP] = "pop";                funcargs[F_POP] = -1;
    funcs[F_POPB] = "popb";              funcargs[F_POPB] = -1;
    funcs[F_REV] = "rev";                funcargs[F_REV] = -1;
    funcs[F_GET] = "get";                funcargs[F_GET] = 1;
    funcs[F_IN] = "in";                  funcargs[F_IN] = 2;
    funcs[F_JOIN] = "join";              funcargs[F_JOIN] = -1;
    funcs[F_TIMES] = "times";            funcargs[F_TIMES] = 2;
    funcs[F_ISDEF] = "isdef";            funcargs[F_ISDEF] = 1;
    funcs[F_ISVAR] = "isvar";            funcargs[F_ISVAR] = 1;
    funcs[F_ISFUNC] = "isfunc";          funcargs[F_ISFUNC] = 1;
    funcs[F_ISBLOCK] = "isblock";        funcargs[F_ISBLOCK] = -1;
    funcs[F_ISINT] = "isint";            funcargs[F_ISINT] = -1;
    funcs[F_LEN] = "len";                funcargs[F_LEN] = 1;
    funcs[F_SPLIT] = "split";            funcargs[F_SPLIT] = 2;
    funcs[F_CSPLIT] = "csplit";          funcargs[F_CSPLIT] = 2;
    funcs[F_IF] = "if";                  funcargs[F_IF] = -1;
    funcs[F_AND] = "and";                funcargs[F_AND] = -1;
    funcs[F_OR] = "or";                  funcargs[F_OR] = -1;
    funcs[F_UNLESS] = "unless";          funcargs[F_UNLESS] = -1;
    funcs[F_ADD] = "add";                funcargs[F_ADD] = -1;
    funcs[F_SUB] = "sub";                funcargs[F_SUB] = -1;
    funcs[F_ABS] = "abs";                funcargs[F_ABS] = 1;
    funcs[F_NEG] = "neg";                funcargs[F_NEG] = 1;
    funcs[F_NOT] = "not";                funcargs[F_NOT] = 1;
    funcs[F_MUL] = "mul";                funcargs[F_MUL] = -1;
    funcs[F_DIV] = "div";                funcargs[F_DIV] = -1;
    funcs[F_MOD] = "mod";                funcargs[F_MOD] = -1;
    funcs[F_CBRT] = "cbrt";              funcargs[F_CBRT] = 1;
    funcs[F_SQRT] = "sqrt";              funcargs[F_SQRT] = 1;
    funcs[F_POW] = "pow";                funcargs[F_POW] = 2;
    funcs[F_LOG] = "log";                funcargs[F_LOG] = 1;
    funcs[F_LN] = "ln";                  funcargs[F_LN] = 1;
    funcs[F_RAND] = "rand";              funcargs[F_RAND] = 0;
    funcs[F_SIN] = "sin";                funcargs[F_SIN] = 1;
    funcs[F_COS] = "cos";                funcargs[F_COS] = 1;
    funcs[F_TAN] = "tan";                funcargs[F_TAN] = 1;
    funcs[F_HEX] = "hex";                funcargs[F_HEX] = 1;
    funcs[F_INT] = "int";                funcargs[F_INT] = 1;
    funcs[F_XOR] = "xor";                funcargs[F_XOR] = -3;
    funcs[F_BAND] = "band";              funcargs[F_BAND] = -3;
    funcs[F_BOR] = "bor";                funcargs[F_BOR] = -3;
    funcs[F_LSHIFT] = "lsh";             funcargs[F_LSHIFT] = -3;
    funcs[F_RSHIFT] = "rsh";             funcargs[F_RSHIFT] = -3;
    funcs[F_SEQ] = "seq";                funcargs[F_SEQ] = -3;
    funcs[F_EQ] = "eq";                  funcargs[F_EQ] = -3;
    funcs[F_NE] = "ne";                  funcargs[F_NE] = -3;
    funcs[F_GT] = "gt";                  funcargs[F_GT] = -3;
    funcs[F_GE] = "ge";                  funcargs[F_GE] = -3;
    funcs[F_LT] = "lt";                  funcargs[F_LT] = -3;
    funcs[F_LE] = "le";                  funcargs[F_LE] = -3;

    // commands
    commands[CMD_SUBCMD] = "=>";           cmdargs[CMD_SUBCMD] = 2;
    commands[CMD_DELSUBCMD] = "!>";        cmdargs[CMD_DELSUBCMD] = 1;
    commands[CMD_QUIT] = "q";              cmdargs[CMD_QUIT] = 0;
    commands[CMD_EXIT] = "exit";           cmdargs[CMD_EXIT] = 1;
    commands[CMD_CD] = "cd";               cmdargs[CMD_CD] = 1;
    commands[CMD_RUN] = "run";             cmdargs[CMD_RUN] = 1;
    commands[CMD_PUTS] = "puts";           cmdargs[CMD_PUTS] = -1;
    commands[CMD_PRINT] = "p";             cmdargs[CMD_PRINT] = -1;
    commands[CMD_ERROR] = "e";             cmdargs[CMD_ERROR] = -1;
    commands[CMD_UNSET] = "unset";         cmdargs[CMD_UNSET] = -2;
    commands[CMD_SOURCE] = "source";       cmdargs[CMD_SOURCE] = 1;
    commands[CMD_VARS] = "vars";           cmdargs[CMD_VARS] = -1;
    commands[CMD_EVERY] = "every";         cmdargs[CMD_EVERY] = 2;
    commands[CMD_AFTER] = "after";         cmdargs[CMD_AFTER] = 2;
    commands[CMD_WAIT] = "wait";           cmdargs[CMD_WAIT] = 1;
    commands[CMD_IF] = "if";               cmdargs[CMD_IF] = 2;
    commands[CMD_UNLESS] = "unless";       cmdargs[CMD_UNLESS] = 2;
    commands[CMD_RETURN] = "return";       cmdargs[CMD_RETURN] = -1;
    commands[CMD_ELSE] = "else";           cmdargs[CMD_ELSE] = 1;
    commands[CMD_WHILE] = "while";         cmdargs[CMD_WHILE] = 2;
    commands[CMD_UNTIL] = "until";         cmdargs[CMD_UNTIL] = 2;
    commands[CMD_FOR] = "for";             cmdargs[CMD_FOR] = -4;
    commands[CMD_BREAK] = "break";         cmdargs[CMD_BREAK] = 0;
    commands[CMD_PASS] = "pass";           cmdargs[CMD_PASS] = -1;

    // general signals
    sigs[SIG_EXIT] = "q";
    sigs[SIG_TERM] = "t";

    kids = alloc(map_strint_t);
    sids = alloc(map_strint_t);
    fids = alloc(map_strint_t);

    kids->strcmp = 1;
    sids->strcmp = 1;
    fids->strcmp = 1;

    // functions
    for (e=FIDS_START; e<FIDS_END; ++e)
        fids->insert(fids, funcs[e], e);
    // special commands
    for (e=KIDS_START; e<KIDS_END; ++e)
        kids->insert(kids, commands[e], e);
    // general signals
    for (e=SIDS_START; e<SIDS_END; ++e)
        sids->insert(sids, sigs[e], e);

    if (kids->full(kids))
        kids->rehash(kids);
    if (fids->full(fids))
        fids->rehash(fids);
}
