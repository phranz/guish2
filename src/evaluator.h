/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef EVALUATOR_H
#define EVALUATOR_H

#define EV_QUITCALLED 1
#define EV_BREAK (1 << 1)
#define EV_RETURN (1 << 2)

#define err(S, L, M, ...)                                                          \
    do {                                                                           \
        debug("-- ERROR --");                                                      \
        if (strace->count) {                                                       \
            fprintf(stderr, "<<<< stack trace >>>:\n");                            \
            strline_t* trace;                                                      \
            each(strace, trace,                                                    \
                fprintf(stderr, "  '%s(...)' at '%lu'\n", trace->key, trace->val); \
            );                                                                     \
        }                                                                          \
        if (S && L)                                                                \
            sinfo(S, L);                                                           \
        fprintf(stderr, M, ## __VA_ARGS__);                                        \
        exit(EXIT_FAILURE);                                                        \
    } while (0)

#define warn(S, L, M, ...)                  \
    do {                                    \
        if (S && L)                         \
            sinfo(S, L);                    \
        fprintf(stderr, M, ## __VA_ARGS__); \
    } while (0)

#define assert_block(A, I, F) if (A->type != T_BLOCK) err(A->source, A->lineno, "error, '%s' %s argument must be a block!\n", F, I)
#define assert_notblock(A, I, F) if (A->type == T_BLOCK) err(A->source, A->lineno, "error, '%s' %s argument cannot be a block!\n", F, I)

typedef struct token_t token_t;
typedef struct vec_token_t phrase_t;
typedef struct vec_str_t vec_str_t;
typedef struct vec_strline_t vec_strlint_t;

typedef struct map_strstr_t map_strstr_t;
typedef struct map_strint_t map_strint_t;

typedef struct scope_t {
    map_strtoken_t* memos;
    void (*free)(struct scope_t*);
} scope_t;

typedef struct block_t {
    vec_token_t* data;
    scope_t* env;
    void (*free)(struct block_t*);
} block_t;

scope_t* scope_t_init();
scope_t* copy_scope(scope_t*);

block_t* block_t_init();
block_t* copy_block(block_t*);

void evaluator_init();
void evaluator_free();

vec_token_t* parse(phrase_t*);
size_t eval_expressions(vec_token_t*, vec_token_t*);
size_t eval_statements(vec_token_t*, vec_token_t*);
token_t* repr(token_t*, int);
long tolong(token_t*);
void define(token_t*, token_t*, scope_t*, vec_token_t*);
void schedule();
int trigger(unsigned long, int);

#endif
