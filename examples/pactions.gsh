#!/usr/bin/env -S guish -q
########################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

cont1=|p|
    s'b:0|bg:#fbfbfc'

cblock=|c|
    C

llock=|l|
    <'lock screen'
    s'b:0|bg:#fbfbfc|h:15|a:l'
    o 
    => c {
        @cblock if(@cblock.c, U, C)
    }

plock=|p|
    <<< @cblock <<< @llock s'b:0|bg:#fbfbfc'
@cont1 <<< @plock

cont2=|p|
    s'b:0|bg:#fbfbfc'

lock=|b|
    <lock => c{
        run if(@cblock.c, 'slock', 'echo')
        q
    }s'b:1|bc:gray|bg:#fbfbfc'

suspend=|b|
    <suspend 
    => c {
        if @cblock.c {
            run 'slock &'
        }
        run 'sudo zzz'
        q
    }s'b:1|bc:gray|bg:#fbfbfc'

reboot=|b|
    <reboot => c{
        run 'sudo reboot'
        q
    }s'b:1|bc:gray|bg:#fbfbfc'

hibernate=|b|
    <hibernate => c{
        if @cblock.c {
            run 'slock &'
        }
        q
    }s'b:1|bc:gray|bg:#fbfbfc'

shutdown=|b|
    <shutdown => c{
        run 'sudo poweroff'
        q
    }s'b:1|bc:gray|bg:#fbfbfc'


@cont2 <<< @lock <<< @suspend <<< @reboot <<< @hibernate <<< @shutdown
main=|p|
    v
    <<< @cont1 <<< @cont2 :'Power Actions'

@main 
n
+
s'm:r'
t
@cont1 o,w
@cont2 o
@main o
