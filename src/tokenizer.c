/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "dectypes.h"
#include "tokenizer.h"
#include "evaluator.h"
#include "sourcedriver.h"
#include "main.h"
#include "debug.h"

#define endtoks "\n\r\v\f;#"
#define nltoks "\n\r\v\f"
#define intoks " \t,"

#define CASE_NLTOKS \
    case '\n':      \
    case '\r':      \
    case '\v':      \
    case '\f'

#define CASE_INTOKS \
    case ' ':       \
    case '\t':      \
    case ','

extern char buf[BSIZ];
extern sourcedata_t* current;

token_t* copy_token(token_t* t) {
    token_t* nt = alloc(token_t);
    nt->type = t->type;
    nt->lineno = t->lineno;
    nt->source = t->source;
    if (t->type == T_BLOCK) {
        nt->block = copy_block(t->block);
    } else {
        nt->data = t->data ? salloc(t->data) : NULL;
    }
    return nt;
}

void token_t_free(token_t* t) {
    if (t->type == T_BLOCK)
        release(t->block);
    t->source = NULL;
    zfree(t->data);
    free(t);
}

token_t* token_t_init(token_t* t) {
    if (!t)
        return NULL;
    memset(t, 0, sizeof(token_t));

    t->free = token_t_free;
    t->type = T_CMD;
    return t;
}

phrase_t* copy_phrase(phrase_t* p) {
    if (!p)
        return NULL;
    token_t* t;
    phrase_t* np = alloc(vec_token_t);
    each(p, t, np->push(np, copy_token(t)););
    return np;
}

void release_phrase(vec_token_t* p) {
    if (p) {
        token_t* t;
        while (p->pop(p, &t)) {
            if (t->type == T_BLOCK)
                release(t->block);
            release(t);
        }
    }
    release(p);
}

#define return_phrase(w, p)                                                 \
    do {                                                                    \
        if (w && w->data)                                                   \
            p->push(p, w);                                                  \
        if (p && p->count) {                                                \
            if (code || expr || slice || elem) {                            \
                sinfo(current->n, current->lineno ? current->lineno-1 : 1); \
                fprintf(stderr, "error, unterminated %s!\n",                \
                    code ? "block" :                                        \
                    expr ? "expression" :                                   \
                    slice ? "slice expression" :                            \
                    elem ? "element expression" :                           \
                           ":-)");                                          \
                exit(EXIT_FAILURE);                                         \
            }                                                               \
            debug("phrase returned\n");                                     \
            return p;                                                       \
        }                                                                   \
        release(w);                                                         \
        release_phrase(p);                                                  \
        p = NULL;                                                           \
    } while (0)

#define push_existing_tok(w, e)              \
    do {                                     \
        if (w) {                             \
            if (w->data && *w->data) {       \
                w->lineno = current->lineno; \
                p->push(p, w);               \
                w = NULL;                    \
            } else if (e) {                  \
                w->data = salloc("");        \
                w->lineno = current->lineno; \
                p->push(p, w);               \
                w = NULL;                    \
            } else {                         \
                release(w);                  \
            }                                \
        }                                    \
    } while (0)

#define push_existing_tok_of_type(w, tp, e)  \
    do {                                     \
        if (w) {                             \
            if (w->data && *w->data) {       \
                w->lineno = current->lineno; \
                p->push(p, w);               \
                w->type = tp;                \
                w = NULL;                    \
            } else if (e) {                  \
                w->data = salloc("");        \
                w->lineno = current->lineno; \
                p->push(p, w);               \
                w->type = tp;                \
                w = NULL;                    \
            } else {                         \
                release(w);                  \
            }                                \
        }                                    \
    } while (0)

#define push_existing_tok_and_create(w, e)   \
    do {                                     \
        if (w) {                             \
            if (w->data && *w->data) {       \
                w->lineno = current->lineno; \
                p->push(p, w);               \
                w = NULL;                    \
            } else if (e) {                  \
                w->data = salloc("");        \
                w->lineno = current->lineno; \
                p->push(p, w);               \
                w = NULL;                    \
            } else {                         \
                release(w);                  \
            }                                \
        }                                    \
        w = alloc(token_t);                  \
        w->lineno = current->lineno;         \
        w->source = current->n;              \
    } while (0)

#define pbuf(bp, d, c)                   \
    do {                                 \
        if (bp == buf+(sizeof(buf)-2)) { \
            *bp = c;                     \
            d = sadd(d, buf);            \
            bp = buf;                    \
            memset(buf, 0, sizeof(buf)); \
        } else {                         \
            *bp++ = c;                   \
        }                                \
    } while (0)

#define pdata(w)                          \
    do {                                  \
        if (w && *buf)                    \
            w->data = sadd(w->data, buf); \
        bp = buf;                         \
        memset(buf, 0, sizeof(buf));      \
    } while (0)

phrase_t* tokenize() {
    char* bp = buf;
    phrase_t* p = NULL;
    head_t* h = NULL;
    token_t* w = NULL;
    int o;
    int nl = 0;
    int code = 0;
    int expr = 0;
    int slice = 0;
    #ifdef ENABLE_X11
    int elem = 0;
    #else
    #define elem (0)
    #endif

    memset(buf, 0, sizeof(buf));
    while ((h = pull())) {
        if (h->c == EOF) {
            if (code || expr || slice || elem) {
                if (current->nb) {
                    continue;
                } else {
                    sinfo(current->n, current->lineno);
                    fprintf(stderr, "error, unterminated %s!\n",
                        code ? "block" :
                        expr ? "expression" :
                        slice ? "slice expression" :
                        elem ? "element expression" :
                               ":-)");
                    exit(EXIT_FAILURE);
                }
            } else if (p) {
                pdata(w);
                return_phrase(w, p);
            }
            break;
        }
        if (!valid(h->c)) {
            sinfo(current->n, current->lineno);
            fprintf(stderr, "error, invalid character '0x%x'.\n", h->c);
            exit(EXIT_FAILURE);
        }
        if (!w && !oneof(h->c, intoks endtoks))
            push_existing_tok_and_create(w, 0);
        if (!p && !oneof(h->c, endtoks))
            p = alloc(vec_token_t);
        switch (h->c) {
        CASE_NLTOKS:
            pdata(w);
            push_existing_tok(w, 0);
            if ((h = pull()) && (h->c != EOF || current->nb)) {
                if (h->c != EOF)
                    push(h->c);
                if (expr || slice || elem) {
                    push_existing_tok(w, 0);
                    ++current->lineno;
                    continue;
                }
                if (code) {
                    token_t* e;
                    if (!p->last(p, &e) || e->type == T_END) {
                        ++current->lineno;
                        continue;
                    }
                    push_existing_tok_and_create(w, 0);
                    pbuf(bp, w->data, '\n');
                    pdata(w);
                    push_existing_tok_of_type(w, T_END, 0);
                    ++current->lineno;
                    continue;
                }
            }
            ++current->lineno;
            return_phrase(w, p);
            break;
        case '#':
            pdata(w);
            push_existing_tok(w, 0);
            if ((h = pull()) && (h->c == '[')) {
                while ((h = pull()) && (h->c != EOF)) { 
                    if (h->c == ']') { 
                        if ((h = pull()) && (h->c == '#' || h->c == EOF))
                            break;
                        push(h->c);
                        if (oneof(h->c, nltoks))
                            ++current->lineno;
                    }
                    if (oneof(h->c, nltoks))
                        ++current->lineno;
                }
                break;
            }
            push(h->c);
            while ((h = pull()) && (h->c != EOF && !oneof(h->c, nltoks)));
            if (oneof(h->c, nltoks))
                push(h->c);
            break;
        case '/':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            if (!((h = pull())) || h->c == EOF) {
                pbuf(bp, w->data, '/');
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            pbuf(bp, w->data, '/');
            if (h->c == '/') {
                pbuf(bp, w->data, h->c);
                pdata(w);
                push_existing_tok_of_type(w, T_CMD, 0);
            } else {
                push(h->c);
                pdata(w);
                push_existing_tok_of_type(w, T_CMD, 0);
            }
            break;
        case '@':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            if (!((h = pull())) || h->c == EOF) {
                pbuf(bp, w->data, '@');
                pdata(w);
                push_existing_tok_of_type(w, T_VARSUB, 0);
                continue;
            }
            pbuf(bp, w->data, '@');
            if (h->c == '*') {
                pbuf(bp, w->data, h->c);
                pdata(w);
                push_existing_tok_of_type(w, T_ARGSSUB, 0);
            } else {
                push(h->c);
                pdata(w);
                push_existing_tok_of_type(w, T_VARSUB, 0);
            }
            break;
        case '&':
        case ':':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, h->c);
            pdata(w);
            push_existing_tok_of_type(w, T_CMD, 0);
            break;
        case '.':
            if (!((h = pull())) || h->c == EOF) {
                pbuf(bp, w->data, '.');
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            if (isdigit(h->c)) {
                pbuf(bp, w->data, '.');
                pbuf(bp, w->data, h->c);
                continue;
            }
            #ifdef ENABLE_X11
            int ttype = T_ATTR;
            #else
            int ttype = T_CMD;
            #endif
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, '.');
            if (0) {
            #ifdef ENABLE_X11
            } else if (h->c == '=') {
                pbuf(bp, w->data, h->c);
                ttype = T_DEFATTR;
            #endif
            } else if (h->c == '.') {
                pbuf(bp, w->data, h->c);
                ttype = T_RANGE;
            } else {
                push(h->c);
            }
            pdata(w);
            push_existing_tok_of_type(w, ttype, 0);
            break;
        #ifdef ENABLE_X11
        case '+':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, h->c);
            pdata(w);
            push_existing_tok_of_type(w, T_CMD, 0);
            break;
        case '*':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, h->c);
            pdata(w);
            push_existing_tok_of_type(w, T_GLOB, 0);
            break;
        case '!':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            w->type = T_CMD;
            if (!((h = pull())) || h->c == EOF) {
                pbuf(bp, w->data, '!');
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            pbuf(bp, w->data, '!');
            if (h->c == '!') {
                pbuf(bp, w->data, h->c);
            } else if (h->c == '>') {
                pbuf(bp, w->data, h->c);
            } else {
                push(h->c);
            }
            pdata(w);
            push_existing_tok(w, 0);
            break;
        case '>':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            w->type = T_CMD;
            if (!((h = pull())) || h->c == EOF) {
                pbuf(bp, w->data, '>');
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            pbuf(bp, w->data, '>');
            if (h->c == '>') {
                pbuf(bp, w->data, h->c);
                if (!((h = pull())) || h->c == EOF) {
                    pbuf(bp, w->data, '>');
                    pdata(w);
                    push_existing_tok(w, 0);
                    continue;
                }
                if (h->c == '>') {
                    pbuf(bp, w->data, h->c);
                } else {
                    push(h->c);
                }
            } else {
                push(h->c);
            }
            pdata(w);
            push_existing_tok(w, 0);
            break;
        case '<':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            w->type = T_CMD;
            if (!((h = pull())) || h->c == EOF) {
                pbuf(bp, w->data, '<');
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            if (h->c != '(') {
                pbuf(bp, w->data, '<');
                if (h->c == '<') {
                    pbuf(bp, w->data, h->c);
                    if (!((h = pull())) || h->c == EOF) {
                        pbuf(bp, w->data, '<');
                        pdata(w);
                        push_existing_tok(w, 0);
                        continue;
                    }
                    if (h->c == '<') {
                        pbuf(bp, w->data, h->c);
                    } else {
                        push(h->c);
                    }
                } else if (oneof(h->c, "=+-")) {
                    pbuf(bp, w->data, h->c);
                } else {
                    push(h->c);
                }
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            push_existing_tok_and_create(w, 0);
            w->type = T_WIDSUB;
            while ((h = pull()) && h->c != ')') {
                if (h->c == EOF) {
                    sinfo(current->n, nl ? current->lineno-1 : current->lineno);
                    fprintf(stderr, "error, unterminated token, expecting ')'.\n");
                    exit(EXIT_FAILURE);
                }
                nl = 0;
                if (oneof(h->c, nltoks)) {
                    ++current->lineno;
                    nl = 1;
                }
                pbuf(bp, w->data, h->c);
            }
            pdata(w);
            push_existing_tok(w, 0);
            break;
        #endif
        case '-':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            w->type = T_CMD;
            pbuf(bp, w->data, '-');
            if (!((h = pull())) || h->c == EOF) {
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            if (h->c == '>') {
                pdata(w);
                release(w);
            } else {
                pdata(w);
                push_existing_tok(w, 0);
                push(h->c);
            }
            break;
        case '=':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            w->type = T_CMD;
            if (!((h = pull())) || h->c == EOF) {
                pbuf(bp, w->data, '=');
                pdata(w);
                push_existing_tok_of_type(w, T_DEFINE, 0);
                continue;
            }
            pbuf(bp, w->data, '=');
            if (oneof(h->c, "=>")) {
                pbuf(bp, w->data, h->c);
                pdata(w);
                push_existing_tok(w, 0);
                continue;
            }
            push(h->c);
            pdata(w);
            push_existing_tok_of_type(w, T_DEFINE, 0);
            break;
        CASE_INTOKS:
            pdata(w);
            push_existing_tok(w, 0);
            break;
        case '\'':
        case '`':
            o = h->c;
            pdata(w);
            push_existing_tok_and_create(w, 0);
            nl = 0;
            while ((h = pull()) && h->c != o) {
                if (h->c == EOF) {
                    sinfo(current->n, nl ? current->lineno-1 : current->lineno);
                    fprintf(stderr, "error, unterminated token, expecting '%c'.\n", o);
                    exit(EXIT_FAILURE);
                }
                if (oneof(h->c, nltoks)) {
                    ++current->lineno;
                    nl = 1;
                }
                pbuf(bp, w->data, h->c);
            }
            pdata(w);
            push_existing_tok_of_type(w, o == '\'' ? T_QUOTE : T_SHELLSUB, 1);
            break;
        case '"':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            nl = 0;
            while ((h = pull()) && h->c != '"') {
                if (h->c == EOF) {
                    sinfo(current->n, nl ? current->lineno-1 : current->lineno);
                    fprintf(stderr, "error, unterminated token, expecting '\"'.\n");
                    exit(EXIT_FAILURE);
                }
                if (h->c == '\\') {
                    if (!(h = pull()) || h->c == EOF) {
                        sinfo(current->n, nl ? current->lineno-1 : current->lineno);
                        fprintf(stderr, "error, unterminated token, expecting '\"'.\n");
                        exit(EXIT_FAILURE);
                    }
                    switch (h->c) {
                        case 'n': pbuf(bp, w->data, '\n'); break;
                        case 't': pbuf(bp, w->data, '\t'); break;
                        case 'r': pbuf(bp, w->data, '\r'); break;
                        case 'v': pbuf(bp, w->data, '\v'); break;
                        case 'f': pbuf(bp, w->data, '\f'); break;
                        case 'b': pbuf(bp, w->data, '\b'); break;
                        case '"': pbuf(bp, w->data, '"'); break;
                        case '\\': pbuf(bp, w->data, '\\'); break;
                        default:
                            pbuf(bp, w->data, '\\');
                            pbuf(bp, w->data, h->c);
                    }
                    pdata(w);
                    continue;
                }
                nl = 0;
                if (oneof(h->c, nltoks)) {
                    ++current->lineno;
                    nl = 1;
                }
                pbuf(bp, w->data, h->c);
            }
            pdata(w);
            push_existing_tok_of_type(w, T_DQUOTE, 1);
            break;
        #ifdef ENABLE_X11
        case '|':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, '|');
            pdata(w);
            elem = !elem;
            push_existing_tok_of_type(w, T_ELEM, 0);
            break;
        #endif
        case '(':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, '(');
            pdata(w);
            ++expr;
            push_existing_tok_of_type(w, T_EXPR, 0);
            break;
        case ')':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, ')');
            pdata(w);
            --expr;
            push_existing_tok_of_type(w, T_ENDEXPR, 0);
            break;
        case '{':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, '{');
            pdata(w);
            ++code;
            push_existing_tok_of_type(w, T_CODE, 0);
            break;
        case '}':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, '}');
            pdata(w);
            --code;
            push_existing_tok_of_type(w, T_ENDCODE, 0);
            break;
        case '[':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, '[');
            pdata(w);
            ++slice;
            push_existing_tok_of_type(w, T_SLICE, 0);
            break;
        case ']':
            pdata(w);
            push_existing_tok_and_create(w, 0);
            pbuf(bp, w->data, ']');
            pdata(w);
            --slice;
            push_existing_tok_of_type(w, T_ENDSLICE, 0);
            break;
        case '\\':
            o = h->c;
            if ((h = pull())) {
                int isnl = oneof(h->c, nltoks);
                if (!isnl && h->c != EOF) {
                    if (o == '\\' && !oneof(h->c, "xX")) {
                        pbuf(bp, w->data, h->c);
                    } else if (oneof(h->c, "xX")) {
                        char b[3];
                        char c;
                        char* e = NULL;

                        for (size_t i=0; i<2; ++i) {
                            if (!(h = pull()) || h->c == EOF) {
                                sinfo(current->n, w->lineno);
                                fprintf(stderr, "error, bad hex value.\n");
                                exit(EXIT_FAILURE);
                            }
                            b[i] = h->c;
                        }
                        b[2] = 0;
                        errno = 0;
                        c = (char)strtol(b, &e, 16);
                        if (!valid(c) || *e || errno == ERANGE || errno == EINVAL) {
                            sinfo(current->n, w->lineno);
                            fprintf(stderr, "error, bad hex value.\n");
                            exit(EXIT_FAILURE);
                        }
                        pbuf(bp, w->data, c);
                    } else {
                        pbuf(bp, w->data, o);
                        push(h->c);
                    }
                } else if (isnl) {
                    ++current->lineno;
                }
            }
            break;
        case ';':
            pdata(w);
            if (code) {
                token_t* e;
                if (p->last(p, &e) && e->type != T_END) {
                    push_existing_tok_and_create(w, 0);
                    pbuf(bp, w->data, h->c);
                    pdata(w);
                    push_existing_tok_of_type(w, T_END, 0);
                } else {
                    push_existing_tok(w, 0);
                }
                continue;
            }
            return_phrase(w, p);
            break;
        default:
            pbuf(bp, w->data, h->c);
            w->type = T_CMD;
        }
    }
    return NULL;
}
