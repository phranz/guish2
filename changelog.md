## 2.6.11 (2024-12-21)

*  changed author email

## 2.6.10 (2023-05-24)

*  fix types for ARM support; fix errno handling; minor fixes; license GPL3 or later fix [commit](https://codeberg.org/phranz/guish/commit/d3f469056e4d51859220eec1769e7f69b35d67f4)


## 2.6.9 (2023-05-24)

*  fixed LDFLAGS; removed dynamic loading of imlib2 [commit](https://codeberg.org/phranz/guish/commit/c3e0b994a4dd40ee5c9a4a0971b288961eb140b9)


## 2.6.8 (2023-05-22)

*  removed asans for musl compatibility; some other fixes for musl compatibility. [commit](https://codeberg.org/phranz/guish/commit/76db0d7a2674633ce35a9c7ab8805554d89e948e)


## 2.6.7 (2023-04-29)

*  added missing dl flag to the linker when image support is activated [commit](https://codeberg.org/phranz/guish/commit/b5e14570a2819163a3de28284d960d47cdaf8b71)


## 2.6.6 (2023-03-14)

*  major refactor; added 'x' drawing operation to draw pixels (uses XDrawPoints); drawing operations reimplemented and optimized; now draw/fill arcs operations consume all the phrase, accepting arcs parameters as group of 6 numbers; fixed incorrect/missing block parsing in some funcs/commands. [commit](https://codeberg.org/phranz/guish/commit/d7efc6577619e18b028666d4bddfb83e77da0359)


## 2.6.5 (2023-03-09)

*  refactor; removed interruption of evaluation, use blocks instead; removed special cases for 'while' and 'until' (use blocks as conditions if you want to delay the evaluation); removed 'call' function; added 'unless' function; removed 'any' and 'all' functions, they are replaced by 'and' and 'or'; now old bitwise 'and' and 'or' functions are now 'band' and 'bor'; minor fixes; minor optimizations. [commit](https://codeberg.org/phranz/guish/commit/23c55a0d8f69e764d0d29845ab83c6deb6484986)


## 2.6.4 (2023-03-06)

*  now assignment consumes all the phrase: if there is a single value, it simply assigns it to a variable, but if there are more, it creates a block, embeds those values inside it, and then assigns it to the variable; function interpolation now doesn't need return command, and will only evaluate expressions; added the special variable 'args' (a block) to refer to command line/function arguments; removed 'global' command; removed special '&' variable substitution; removed 'slice', 'at' and 'count' functions; fixed 'join' function; minor fixes; changed examples for newer syntax; doc fixes. [commit](https://codeberg.org/phranz/guish/commit/5773b54fcb07b70b7a9fe31af2dfc49ef75bec0b)


## 2.6.3 (2023-03-02)

*  added unregister subcommand command ("!>"); fixed returning from user-made functions. [commit](https://codeberg.org/phranz/guish/commit/fa7d08afd184454925237bba48381bb057c06805)


## 2.6.2 (2023-02-23)

*  added function interpolation in double quoted strings with @(...); fixed block string repr; fixed order of arguments returned by functions; minor doc fixes. [commit](https://codeberg.org/phranz/guish/commit/47ced313f2b5de402641ed43454aeaeb566ccc22)


## 2.6.1 (2023-02-21)

*  changed 'for' command syntax, now it's multivariable; added a new operator to define attributes (.=); added a new operator to express numerical ranges (ex. 1..100) and removed 'range' function; added 'int' function; removed style attribute 'background-image', now its functionality is merged into 'background' attribute (if the value begins with a slash '/', then it's treated as an image file); added an optional block (after element) specifying initial width and height into element expressions (ex. |b{90,90}|); improved wait/poll code; fixe wrong block parsing when tokenizing; added additional examples; minor changes. [commit](https://codeberg.org/phranz/guish/commit/2e1f90351a1275a611a4970cf904dba0101e64d0)


## 2.6.0 (2023-02-18)

*  major refactor; added floating point arithmetic; added transparent (requires compositor) label as a new element; now 'wait' command can take millisecs (specified as fractions of seconds, ex.: 0.040); added raw draw operation command '/' with subcommands to draw line, points, polygons and arcs (for each element); added special variable reference operator '&' that works like '@', but gives error if the variable is empty; added more math functions; added rand and hex function; added 'line' attribute in style string to specify line width in pixels; added additional examples; removed join operator; fixed makefile; fixed join function; minor fixes; refactored tests. [commit](https://codeberg.org/phranz/guish/commit/316fdfe665cf3cb483e865492266a57aa0248322)


## 2.5.1 (2023-01-31)

*  removed dump command; puts, p and e commands now takes all tokens in a phrase; unset command can now unset elements attributes too; vars command can now show elements attributes; new cutils integrated; minor fixes; minor optimizations. [commit](https://codeberg.org/phranz/guish/commit/e3de2a2c30163565f3f90cb8c189d6cc3535b489)


## 2.5.0 (2023-01-24)

*  m [commit](https://codeberg.org/phranz/guish/commit/a6f538705b2cacaa0e6b2dbdce69f01233a7817a)
*  element specific functions are now attributes; reintroduced attributes; now an element can have an 'unlimited' number of custom attributes; removed pointless 'attach' func/command; fixed generic signal code execution. [commit](https://codeberg.org/phranz/guish/commit/d1499fea266b3551f1b9c31879dd0141f995cb96)


## 2.4.8 (2023-01-19)

*  added 'builtin' builtin function to easily override builtin functions; minor memory optimizations and fixes; added some tests. [commit](https://codeberg.org/phranz/guish/commit/fc82a14b1ee7fd87d3d2f781ef65fdc8870ccd1c)


## 2.4.7 (2023-01-15)

*  minor refactor; minor optimizations. [commit](https://codeberg.org/phranz/guish/commit/dff6ea60a1836768d789eea14baf7c862bd1961d)


## 2.4.6 (2023-01-10)

*  new version; fixed/changed makefile; added additional tests; now 'eval' function will accept any number (and type) of arguments; added correct source name on error; minor refactor; some improvements; minor fixes. [commit](https://codeberg.org/phranz/guish/commit/61fbe4676994f64324bb779254d83f13a9bc7b08)


## 2.4.5 (2023-01-09)

*  new version; added '-b' option to show compiled-in build time options; now '@n' and '@*' variable substitution arguments and push, pushb, pop, popb functions, work with command line parameters too; added 'cd' command; added 'eval' and 'cwd' functions; using default options in makefile; fixed send and ctrl commands; makefile dependency fix; minor refactor; removed some tests; fixed docs. [commit](https://codeberg.org/phranz/guish/commit/99b86a5a58b13c563e92cee3cbe13eb9e0564b2d)


## 2.4.4 (2023-01-08)

*  fixed regression of previous 'for' fix :D [commit](https://codeberg.org/phranz/guish/commit/20ece3c8629d37c6fef22e2e2d6d244c7ad4d3c7)


## 2.4.3 (2023-01-08)

*  fixed SIGSEGV with 'for' command; minor unused warning thing suppression [commit](https://codeberg.org/phranz/guish/commit/6edc64437cc4d5d87aa303a193e48fe00aadc949)


## 2.4.2 (2023-01-08)

*  removed autotools, using a simple makefile; minor changes. [commit](https://codeberg.org/phranz/guish/commit/7cb63232c20e67789fb1a89a22a0279d4554d805)
*  removed autotools, using a simple makefile; added build instructions; minor changes. [commit](https://codeberg.org/phranz/guish/commit/12294971902a18774b8f5ce60f343c446c20e596)


## 2.4.1 (2023-01-06)

*  fixed variable substitution token type; fixed style string parsing (use backslashes to puts spaces in values); minor doc fixes; added some error tests runnable with 'make tests' after make all [commit](https://codeberg.org/phranz/guish/commit/6f80f35f65f2881a41434b9b0221b8321aec09a2)


## 2.4.0 (2023-01-04)

*  new version with lot of new features and improvements; major refactor; removed some command line options, renamed some others; added closures; added slicing; added function stack trace error reporting; element expressions are now true expressions; imlib2 support for images is now loaded when needed (not linked at build time); modified some functions to work with both tokens and blocks at the same time; added special ++ operator; removed opts, setopt, leave, i, I, k, K, and at commands; added '>' generic command to define a variable using element data; added 'if' as a function too; lot of fixes and minor improvements. [commit](https://codeberg.org/phranz/guish/commit/70302ba8420591dd21164ebc4f9250fa6cfcef64)


## 2.3.2 (2022-12-16)

*  added global special command; fixed configure options & some ifdefs; correctly detect missing parens/brackets/braces when in expressions/list/code; for page element, when not fitting, if page's width and/or height are greater than the sum of widths/heights of its sub-elements, then page [commit](https://codeberg.org/phranz/guish/commit/38a759962a5f7e97a27feb35d6c7c2b67ca56c9b)


## 2.3.1 (2022-12-05)

*  new version; added 'at', 'true', 'false' and 'isint' functions;  fixed token type returned by some functions; fixed initial x,y coords of elements; fixed doc. [commit](https://codeberg.org/phranz/guish/commit/8b503ac08224ed7039eaf8ce15b68452507fb904)


## 2.3.0 (2022-12-04)

*  new major change: blocks are now first class arguments, and can be passed in custom functions too; major refactor; 'def' function renamed to 'let', removed 'slice' and 'get' functions, 'fetch' function renamed to 'get', 'take' function renamed to 'slice', 'defined' function renamed to 'isdef'; added 'list', 'some' and 'block' functions; removed signals arguments in []; added code subsitution operator '%'; fixed immediate code block execution with arguments; minor fixes. [commit](https://codeberg.org/phranz/guish/commit/7d96fa9f412368587ff181a5f99bc52f98daf541)


## 2.2.6 (2022-11-30)

*  minor fix [commit](https://codeberg.org/phranz/guish/commit/5bd0d2a34d1fbc1376ce3e49bcc766df1849490c)
*  removed aliases support (and its -a option) and -i option; builtin functions can now be passed as the first argument to 'each' function (when builtin, no additional scope is created); added support for variable interpolation inside element expressions and window id substitution; new quit-on-last-closed is triggered with external clients too; fixed an infinite loop bug when using window id substitution; minor refactor; minor code optimizations; fixed builtin functions number of arguments constraints; minor doc fixes. [commit](https://codeberg.org/phranz/guish/commit/be07b798c6289081b5f8497da9996a2b1c26124d)


## 2.2.5 (2022-11-29)

*  added "each" function; removed duplicated pointless files; fixed documentation [commit](https://codeberg.org/phranz/guish/commit/d0433f98d5121316ffc12bbbac125f33e99f6c02)


## 2.2.4 (2022-11-28)

*  new version with minor additions; added swap and invert commands for page element; added additional utils in cutils [commit](https://codeberg.org/phranz/guish/commit/8eef91f74785cef081941a39a90f59c3ffae71b2)


## 2.2.3 (2022-11-24)

*  new version with minor additions; now page elements can use alignment style attribute to align theis subwidgets depending on layout; some docs fixed [commit](https://codeberg.org/phranz/guish/commit/d6b4c0f2b0bdd3b01780d34f828e30dafd2e4d34)


## 2.2.2 (2022-11-22)

*  new version with some fixes and minor changes; now xlib dependency is configurable at build time (but by disabling it, one would do little); now blocks can be passed to call function directly; additional fixes [commit](https://codeberg.org/phranz/guish/commit/97790798e60e25fda016297b1b3ee0ec7b75af91)


## 2.2.1 (2022-11-21)

*  added tail recursion optimization to funcs [commit](https://codeberg.org/phranz/guish/commit/f9a248e6f4513464fb2b5d33555d8be30a652ded)
*  new version; fixed some issues with TCO; added def, push, pushb, pop, popb, functions; blocks are now correctly not evaluated when passing them as arguments to functions; added additional examples [commit](https://codeberg.org/phranz/guish/commit/149bafcbdb88e1bf563b5ccc99fbfcf4db3cd4a4)


## 2.2.0 (2022-11-19)

*  new version with lot of mods; major refactor of evaluation and substitution code; now a code block is not evaluated automatically if it is the first "dynamic" argument, to evaluate/call it, parens must be used, and that can be now done inside expressions too; now "any" and "all" functions implement non-strict evaluation (work like shortcircuits && ||); added rev and fetch functions; added additional function validation rules [commit](https://codeberg.org/phranz/guish/commit/45d59253d1509698a9eed309a0caad64e8e58f2d)


## 2.1.8 (2022-11-12)

*  new version; added any and all functions; minor changes/refactor; fixed man typo [commit](https://codeberg.org/phranz/guish/commit/7bf7f324375672be73e84da66e9156220e230345)


## 2.1.7 (2022-11-11)

*  new version; added optional aliases substitution (from commands only) with newer -a option; removed nargs; added functions head, tail, isvar, isfunc, isalias [commit](https://codeberg.org/phranz/guish/commit/7b43ac58b39d36094faa77695ec872317afaa2b1)


## 2.1.6 (2022-11-10)

*  added some functions: env(to get environment variables), times(to repeat a token n times), count(to count tokens), and bitwise ops and, or, xor, rshift, lshift [commit](https://codeberg.org/phranz/guish/commit/7abf52246470acf26dd11a2dffab22bd3bee3cc2)


## 2.1.5 (2022-11-09)

*  new version with mainly changes and fixes; modified "take", now it is like slice but for tokens; added special operator "@*" which expands to a list of argument when in a function; fixed empty string crashes; improved error handling for functions; fixed code,expr and list handling when multiline and in tty prompt mode; minor improvements/code optimizations; added example [commit](https://codeberg.org/phranz/guish/commit/f8a87f48d325c524cde95cafa39db0fdb325e0a6)


## 2.1.4 (2022-11-07)

*  new version; pass command now consumes all remaining arguments; added list assignment to assign multiple tokens to a variable; added take function to get a token from a list using an index; added arguments to signals code blocks; now function arguments are not limited to 7; added range and nargs functions; fixed issues with cutils map items; some fixes and optimizations [commit](https://codeberg.org/phranz/guish/commit/3bab52450c3f6691c820896c7451257984f09dab)


## 2.1.3 (2022-11-05)

*  new version; added multiline comments with "#[" and "]#" refined argument validation for builting functions; added read, write and append builtin functions; now SIGINT/SIGTERM handler is installed only when required; changed README.md; fixed manual [commit](https://codeberg.org/phranz/guish/commit/87a91e43a98ef978641757246e146baef7578c3a)


## 2.1.2 (2022-11-03)

*  new rev; fixed functions return; added overflow detection for some arithmetic funcs; fixed some issues with relate command (and movealign); minor changes [commit](https://codeberg.org/phranz/guish/commit/1f1b1fd65bc31e1883b99add3a517e3bbf7cc66f)


## 2.1.1 (2022-11-02)

*  fixed some doc [commit](https://codeberg.org/phranz/guish/commit/906096c28b500fd117a526f41be7dc81e321408f)
*  new version; added dump special command to print a whole phrase to stdout; fixed grip/movable events; fixed manual [commit](https://codeberg.org/phranz/guish/commit/943cb8ec5587cbe02fa5ab1864563336f777611a)


## 2.1.0 (2022-10-31)

*  use tokens directly to remember values, instead of 2 maps (one for data and one for type) [commit](https://codeberg.org/phranz/guish/commit/63acba41202e187bfe06f0ddc3786a0b695099e3)
*  initial code to add real code blocks (removing eval sources from sourcedriver); tty mode now waits correctly on partial code block implementation [commit](https://codeberg.org/phranz/guish/commit/602e9aa0ece401ed6e0e3810168ed475981e6058)
*  new if code [commit](https://codeberg.org/phranz/guish/commit/fdcd287552ec78777480f4d6fe45e021b8a5bb28)
*  added else code too [commit](https://codeberg.org/phranz/guish/commit/a1414031dc5a8a74125432c01dbb8e03d73a8cbb)
*  newer version 2.1.0 with lot of new features, changes and bug fixes; hex substitution is done directly by the tokenizer; escaping is done directly by the tokenizer, but just on double quoted strings; now all special substitution are done in a single pass inside the pushdown automata; removed math substitution with brackets, now all expression are functions; assignment is a piece of eval_special now; functions can now return multiple values using "return" special command; dot substitution syntax removed, now all its features are functions; added a lot of new utility functions like "join", "split", etc.; some special operators like "&" and "?" removed, now they are functions (see man); code blocks now are composed by multiple tokens and cannot be used like strings like in previous versions; string interpolation works only when using shell substitution and double quotes; single quoted strings are always raw; removed GLOBCMD **; now parens are not ignored, they serve as a marker to the pushdown automata to call functions and resolve expressions; added "-i" option to disable automatic variable string interpolation; to call a function parens must be used, not var substitution using "@" (using it with a function, substitutes the name of the function); added "FILE" special variable to get the path of current source; optimized window event handling; lot of fixes; lot of leaks fixed [commit](https://codeberg.org/phranz/guish/commit/5e21b861952be16f9efb307dea6ba451845861f4)


## 2.0.6 (2022-10-15)

*  fixed defined operator (scopes) [commit](https://codeberg.org/phranz/guish/commit/c3ea4bff460d5e3d40487437e4bea7a1762c9fcd)
*  fixed segfault on empty & arg [commit](https://codeberg.org/phranz/guish/commit/c8df2a077d60f233c49a7a0d146b93b07c0f3e53)
*  fixed draw regression when pressing/releasing; fixed error reporting lineno on vsub [commit](https://codeberg.org/phranz/guish/commit/d4c4ddd41614d69448fe66305522d696d6b264f4)
*  new version; fixed/reimplemented naive "if/else" implementation; added empty token support in the language;  added shorthand (ex. a=) to define empty variables; removed/changed xsub, now it is in the tokenizer; added fitembed (<<<) and fituntie (>>>) commands; changed some command name (see manual page); simplified arithmetic expression, now there are no clashes between operators; now "leave" command jumps out of all blocks except loops; added "self" special variable in signals; added click "c" command; added "a" command to add attach custom data to a widget; in dot subsitution added: "a" to get attached data, "t" to get title of a widget, "T" to get the type of a widget, "p" to get pid of a widget (now works with same program too); fixed additional leaks/bugs; refactored/improved [commit](https://codeberg.org/phranz/guish/commit/118e4b72f27bad36d7926c07cd7d6224dec502f3)


## 2.0.4 (2022-10-09)

*  new version; added leave special command to "exit" a conditional block on a condition; added scopes (now there exist local variables); loops fixed; fixed various leaks; improved [commit](https://codeberg.org/phranz/guish/commit/33b622cf6f59bc7563f3d7a857c0eadd22b03d25)


## 2.0.3 (2022-10-08)

*  new version; re-changed default expansion mode for page (fixed); added unless and until commands; now else can be used after while too; added nop pass command; in dot substitution, added the following attributes: "t" which gives element type, "s" which expands to the ids of subwidgets of a page (one token per id), "n" which returns the number of subwidgets of a page; added "$" function to func sub, it splits the data by spaces, returning multiple tokens; added "double" (indirect) reference operator "@@" to reference variables using a variable name saved in another variable; now when deleting page element, all its subwidgets are deleted too; fixed coords issue when embedding in a page; fixed/improved cutils [commit](https://codeberg.org/phranz/guish/commit/7c0651f6f197802b3a633d5ca92d618253ea481b)


## 2.0.2 (2022-10-05)

*  added left/middle/right/scroll-up/scroll-down mouse signals; added focused-unfocused signals; in dot attributes, added "f" and "u" to get focus status; fixed bug and memory leak when unsetting schedules (and with every/after commands); glob command now do not force generated token type to T_CODE; fixed a bug in string comparison; now daemon call does not chdir to root dir; now in file/source mode if the file specified is a named pipe, opens it read/write (so one have not to open it for writing in another place like for ex. exec 3<>pipe); fixed retrieving tray window coords; now hidesubs is executed after page hide [commit](https://codeberg.org/phranz/guish/commit/d37dcc999aa6444e908fe6e2b3f76e602e1b0782)


## 2.0.1 (2022-10-01)

*  fixed/added missing header config.h [commit](https://codeberg.org/phranz/guish/commit/7c33d701555697e43778beedf6a552db998bbcd2)
*  add an addition example [commit](https://codeberg.org/phranz/guish/commit/327ee98ee5d115891c8e9a4712259aff2359c6db)
*  fixed BadWindow error when spawning new xterms in splitterm example [commit](https://codeberg.org/phranz/guish/commit/3a73d762bdf9a154c57e1a0049560d02db8cf5bb)
*  refactor; added enter/leave generic signals; added hovering color styles; now there is just one graphic context [commit](https://codeberg.org/phranz/guish/commit/3bfe14f5811541a8ecba4bcf28f35bcaa25ba564)
*  fixed/changed dot substitution implementation; fixed deletion of an element by a signal triggered by itself (use a delete later strategy) [commit](https://codeberg.org/phranz/guish/commit/ef1a2436254f5aa2f3c0d029ff46b602f362df11)
*  corrected some errors inherited by old documentation (1.x) [commit](https://codeberg.org/phranz/guish/commit/fff6e8b0016a0c7e062955055a070820491c7fd8)
*  removed showsubs and hidesubs page commands; added token joining with operator "++"; renamed set top and bottom from ^_ to tb [commit](https://codeberg.org/phranz/guish/commit/98d362c8f5bc3224fcb67bd9f11e2e30bcebd357)
*  Major refactor; changed documentation; added 'wait' command; added while and for loops, added break command; changed substitution order; using a new version of cutils; added new func substitution (added funcs 'def' and 'len', to respectively check if a variable si defined and get the character length of a string); many generic fixes; fixed problem when defining multiple variables from command line using -v option; improved input element (cursor visible); fixed/improved 'fit' command; added per widget configurable margins. [commit](https://codeberg.org/phranz/guish/commit/cbcadf1e459d6bd74d5fe2e690e37fbb18309465)
*  added relate command [commit](https://codeberg.org/phranz/guish/commit/95e2694db09f6c2aad20ed7e8be6cdeebf5520f5)
*  added image background support through Imlib2 (optional), now glob operator '*' simply returns all widgets ids (old operator behaviour is given by new operator '**'), changed names of some normal commands (see manual page), send and ctrl commands are now special commands instead of generics, improved input widget (cursor support, in-string insertion and multiline support through new command 'W'), len is now '&', def is now '?', added string comparison operators '==''!=' and 'in', refactored/improved/fixed cutils [commit](https://codeberg.org/phranz/guish/commit/02b807afb0b4105c6ae06e00128ca164f7fbd39d)
*  default mode for page element is now relaxed; minor changes to examples to make them work well under different window managers [commit](https://codeberg.org/phranz/guish/commit/623a1ba88aa8a7170b0058f4b3275a3157065fc4)
*  refactored; changed math expression syntax from infix to prefix form; now show and hide on a page will act on all subelements too; added "S" command to style subelements of a page; added special signal "t" that is triggered when sending SIGINT of SIGTERM to guish; added "v" and "e" attributes in dot substitution to respectively to check for visibility of an element and element freeze/unfreeze state; now, in math expressions, unary minus and plus are respectively "neg" and "plus"; added go-in-system-tray command "T"; generic improvemens/fixes [commit](https://codeberg.org/phranz/guish/commit/a0f125ebb54d9dd24eec9f4ff2920a1972e81711)
*  added daemonize option; removed bad funcs from cutils; fixed named pipe recognition [commit](https://codeberg.org/phranz/guish/commit/e7de3483fdf020645b60dc03d52f6c0dc8c7e892)
*  do not center new widgets by default [commit](https://codeberg.org/phranz/guish/commit/5760fdfa50be5b38edc6d2d98674a2da27166ed2)
*  more stable version 2.0.1; fixed SEGFAULT in eval_code [commit](https://codeberg.org/phranz/guish/commit/fc1de4b808c98eb0eec7bee9a1d1e483ecf7812c)


