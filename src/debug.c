/************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <stdio.h>

#include "dectypes.h"
#include "debug.h"
#include "tokenizer.h"
#include "evaluator.h"

void printoken(token_t* t, const char* fl, const char* f, int l) {
    if (!t)
        return;
    TOKNAMES();
    fprintf(stderr, "[\033[0;33m%s\033[0m][\033[1;33m%s\033[0m]('%d') TOK:", fl, f, l);
    fprintf(stderr, "\033[1;32m%s\033[0m(t:%s)(l:%llu)(p:%p)",
        t->data, toknames[t->type], t->lineno, (void*)t->data);
}

void printokens(vec_token_t* toks, int ft, const char* fl, const char* f, int l) {
    if (!toks)
        return;
    token_t* t;
    TOKNAMES();
    if (ft)
        fprintf(stderr, "[\033[0;33m%s\033[0m][\033[1;33m%s\033[0m]('%d') TOKENS:", fl, f, l);
    for (size_t i=0; i<toks->count; ++i) {
        toks->get(toks, i, &t);
        if (t->type != T_BLOCK) {
            fprintf(stderr, "\033[1;32m%s\033[0m(t:%s)(l:%llu)(p:%p)",
                t->data, toknames[t->type], t->lineno, (void*)t->data);
        } else {
            printokens(t->block->data, 0, fl, f, l);
            //fprintf(stderr, "\n");
        }
    }
    fprintf(stderr, "\n");
}
