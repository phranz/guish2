/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (syntax_t*, at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef SYNTAX_H
#define SYNTAX_H

// ends are excluded

#define FIDS_START F_READ
#define FIDS_END F_END
#define KIDS_START CMD_SUBCMD

#ifdef ENABLE_X11
typedef enum wattrs {
    ATTR_NONE,
    ATTR_W,
    ATTR_H,
    ATTR_X,
    ATTR_Y,
    ATTR_B,
    ATTR_G,
    ATTR_D,
    ATTR_C,
    ATTR_V,
    ATTR_E,
    ATTR_F,
    ATTR_S,
    ATTR_N,
    ATTR_TL,
    ATTR_T,
    ATTR_PID,
    ATTR_END
} wattrs;
    #define EIDS_START E_BUTTON
    #define EIDS_END E_END
    #define KIDS_END CMD_TRAY
    #define CIDS_START KIDS_END
    #define CIDS_END CMD_END
#else
    #define KIDS_END CMD_END
#endif

#define SIDS_START SIG_EXIT
#define SIDS_END SIG_ENTER

typedef enum element {
    E_NONE,
    E_ALL,
    E_BUTTON,
    E_INPUT,
    E_PAGE,
    E_LABEL,
    E_CHECKBOX,
    E_TRANS,
    E_END,
} element;

typedef enum func {
    F_NONE,
    F_READ,
    F_WRITE,
    F_APPEND,
    F_ENV,
    F_CWD,
    F_PUTS,
    F_BLOCK,
    F_FLAT,
    F_EVAL,
    F_BUILTIN,
    F_EACH,
    F_LET,
    F_SOME,
    F_TRUE,
    F_FALSE,
    F_PUSH,
    F_PUSHB,
    F_POP,
    F_POPB,
    F_REV,
    F_TIMES,
    F_GET,
    F_IN,
    F_JOIN,
    F_ISDEF,
    F_ISVAR,
    F_ISFUNC,
    F_ISBLOCK,
    F_ISINT,
    F_LEN,
    F_SPLIT,
    F_CSPLIT,
    F_IF,
    F_AND,
    F_OR,
    F_UNLESS,
    F_ADD,
    F_SUB,
    F_ABS,
    F_NEG,
    F_NOT,
    F_MUL,
    F_DIV,
    F_MOD,
    F_CBRT,
    F_SQRT,
    F_POW,
    F_LOG,
    F_LN,
    F_RAND,
    F_SIN,
    F_COS,
    F_TAN,
    F_HEX,
    F_INT,
    F_XOR,
    F_BAND,
    F_BOR,
    F_LSHIFT,
    F_RSHIFT,
    F_SEQ,
    F_EQ,
    F_NE,
    F_GT,
    F_GE,
    F_LT,
    F_LE,
    #ifdef ENABLE_X11
    F_EXISTS,
    #endif
    F_END,
} func;

typedef enum cmd {
    CMD_NONE,
    CMD_SUBCMD,
    CMD_DELSUBCMD,
    CMD_QUIT,
    CMD_EXIT,
    CMD_CD,
    CMD_RUN,
    CMD_PUTS,
    CMD_PRINT,
    CMD_ERROR,
    CMD_UNSET,
    CMD_SOURCE,
    CMD_VARS,
    CMD_EVERY,
    CMD_AFTER,
    CMD_WAIT,
    CMD_IF,
    CMD_UNLESS,
    CMD_RETURN,
    CMD_ELSE,
    CMD_WHILE,
    CMD_UNTIL,
    CMD_FOR,
    CMD_BREAK,
    CMD_PASS,
    #ifdef ENABLE_X11
    CMD_WIDS,
    CMD_DELETE,
    CMD_RELATE,
    #ifdef ENABLE_CONTROL
    CMD_SENDKEYS,
    CMD_SENDCONTROLKEYS,
    #endif
    CMD_TRAY, // common
    CMD_GHOST,
    CMD_FULLSCREEN,
    CMD_SETDEFAULT,
    CMD_BYPASSWM,
    CMD_FOLLOWWM,
    CMD_CENTER,
    CMD_HIDE,
    CMD_SHOW,
    CMD_DRAW,
    CMD_FOCUS,
    CMD_TOP,
    CMD_BOTTOM,
    CMD_CLOSE,
    CMD_CLICK,
    CMD_LOWER,
    CMD_RAISE,
    CMD_MAXIMIZE,
    CMD_FREEZE,
    CMD_UNFREEZE,
    CMD_FIT,
    CMD_FILL,
    CMD_FILLNOLIMIT,
    CMD_FILLRIGHT,
    CMD_FILLRIGHTNOLIMIT,
    CMD_FILLBOTTOM,
    CMD_FILLBOTTOMNOLIMIT,
    CMD_CLEAR,
    CMD_ENTITLE,
    CMD_STYLE,
    CMD_RESIZE,
    CMD_MOVE,
    CMD_MOVEALIGN,
    CMD_TXMOVABLE,
    CMD_TYMOVABLE,
    CMD_SETTEXT,
    CMD_ADDTEXT,
    CMD_DEFTEXT,
    CMD_TGRIP, // end common
    CMD_CHECK, // checkbox
    CMD_UNCHECK,
    CMD_SHOWINPUT, // input
    CMD_HIDEINPUT,
    CMD_PASSWORD,
    CMD_TRET,
    CMD_EQUALIZE, // page
    CMD_STYLESUBS,
    CMD_EXPLODE,
    CMD_EMBED,
    CMD_FITEMBED,
    CMD_UNTIE,
    CMD_FITUNTIE,
    CMD_VERTICAL,
    CMD_HORIZONTAL,
    CMD_SWAP,
    CMD_INVERT,
    #endif
    CMD_END,
} cmd;

typedef enum sig {
    SIG_NONE,
    SIG_EXIT,
    SIG_TERM,
    // for widgets
    SIG_ENTER,
    SIG_LEAVE,
    SIG_FOCUS,
    SIG_UNFOCUS,
    SIG_CLOSED,
    SIG_CLICKED,
    SIG_LCLICKED,
    SIG_RCLICKED,
    SIG_MCLICKED,
    SIG_DCLICKED,
    SIG_LDCLICKED,
    SIG_RDCLICKED,
    SIG_MDCLICKED,
    SIG_PRESSED,
    SIG_LPRESSED,
    SIG_RPRESSED,
    SIG_MPRESSED,
    SIG_RELEASED,
    SIG_LRELEASED,
    SIG_RRELEASED,
    SIG_MRELEASED,
    SIG_SCROLLUP,
    SIG_SCROLLDOWN,
    SIG_MOVED,
    SIG_RESIZED,
    SIG_UNCHECKED,
    SIG_CHECKED,
    SIG_RETURNPRESSED,
    SIG_END,
} sig;

int gsid(char*);
int sid(int, char*);
int eid(const char*);
int cidbe(int, char*);
int kid(const char*);
int cid(const char*);
int fid(const char*);
int aid(const char*);
int cargs(int);
int fargs(int);
const char* ename(int);
const char* fname(int);
int sidbe(int, char*);

void syntax_init();
void syntax_free();

#endif
