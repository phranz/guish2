#!/bin/sh -e
#######################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################
 
if test "$LEAKS" = 1
then
    printf -- '\n-------------------------------\n'
    printf -- 'Testing oneliners/scripts for leaks\n'
    printf -- '-----------------------------------\n\n'
else
    printf -- '\n========================\n'
    printf -- 'Running functional tests\n'
    printf -- '========================\n\n'
fi

. tests/utils.sh

tfunc=otest
test "$LEAKS" = 1 && tfunc=ltest

$tfunc -c 'puts "here:@(1..5)!"' 'here:1 2 3 4 5!'

$tfunc "" 'examples/fact.gsh' 5040
$tfunc "" 'examples/tco_fact.gsh' 5040
$tfunc "" 'examples/mapadd.gsh' '3 4 5 6 7 8 9'

printf '\n'
