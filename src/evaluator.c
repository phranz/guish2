/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <signal.h>
#include <math.h>

#include "main.h"
#include "dectypes.h"
#include "cutils.h"
#include "sourcedriver.h"
#include "evaluator.h"
#include "parser.h"
#include "tokenizer.h"
#include "syntax.h"
#include "debug.h"

#ifdef ENABLE_X11
#include "widgets.h"
#include "exec.h"
#else
#define exec(X) (0)
#define display 0
#endif

extern char buf[BSIZ];

extern int exit_status;
extern sourcedata_t* current;
extern unsigned long options;

#ifdef ENABLE_X11

extern map_widwidget_t* widgets;
extern map_pidwid_t* exts;

extern Atom delatom;
extern int dfd;
extern Display* display;
extern Window root;
extern int screen;
extern map_strcol_t* colors;

map_widloci_t* widlines;
map_widloci_t* widareas;
map_widarcssets_t* widarcssets;
map_widarcssets_t* widarcsareas;
map_widloci_t* widloci;
map_widloci_t* widpixels;
map_widnamedpoints_t* widnamedpoints;

map_wid_sigcodes_t* widacts;
map_widscope_t* wscopes;
vec_wid_t* todel;
widget_t* it;
#endif


volatile sig_atomic_t sq;
map_sigtoken_t* actions;
scope_t* cs;

static scopes_t* scopes;
static vec_int_t* conds;
static int lc;
static unsigned long long wsig;

vec_strline_t* strace;
map_int_timetoken_t* schedules;
map_int_timetoken_t* onetimes;

unsigned long flags;

#define EXITLOOP() ((flags & (EV_BREAK|EV_RETURN)))
#define MUSTQUIT() (sq || flags & EV_QUITCALLED)

#define NEWSCOPE()                            \
    do {                                      \
        scopes->push(scopes, alloc(scope_t)); \
        scopes->last(scopes, &cs);            \
        debug("<SCOPE: %p\n", (void*)cs);     \
    } while (0)

#define EXITSCOPE()                       \
    do {                                  \
        scope_t* s = NULL;                \
        scopes->pop(scopes, &s);          \
        debug(">SCOPE: %p\n", (void*)s);  \
        release(s);                       \
        scopes->last(scopes, &cs);        \
        debug("=SCOPE: %p\n", (void*)cs); \
    } while (0)

#define empty(t) (!*t->data)

void term_handler(int s) {
    if (s != SIGINT && s != SIGTERM)
        return;
    sq = 1;
}

long tolong(token_t* t) {
    long r = 0;
    char* e = NULL;

    if (empty(t)) {
        err(t->source, t->lineno, "error, empty token instead of number!\n");
        return 0;
    }
    errno = 0;
    r = (long)strtod(t->data, &e);
    if (*e || errno == EINVAL) {
        err(t->source, t->lineno, "error, invalid number '%s'\n", t->data);
        return 0;
    }
    if (errno == ERANGE) {
        err(t->source, t->lineno, "error, number is out of range!\n");
        return 0;
    }
    return r;
}

double todouble(token_t* t) {
    double r = 0;
    char* e = NULL;

    if (empty(t)) {
        err(t->source, t->lineno, "error, empty token instead of number!\n");
        return 0;
    }
    errno = 0;
    r = strtod(t->data, &e);
    if (*e || errno == EINVAL) {
        err(t->source, t->lineno, "error, invalid number '%s'\n", t->data);
        return 0;
    }
    if (errno == ERANGE) {
        err(t->source, t->lineno, "error, number is out of range!\n");
        return 0;
    }
    return r;
}

static int true(token_t* a) {
    return a->type != T_BLOCK ? (!empty(a) && !eq(a->data, "0")) : a->block->data->count > 2;
}

#ifdef ENABLE_X11
static void register_widaction(Window w, sig s, token_t* o) {
    if (!widacts)
        widacts = alloc(map_wid_sigcodes_t);
    vec_sigtoken_t* a;
    sigtoken_t* sc;

    if (widacts->full(widacts))
        widacts->rehash(widacts);
    if (!widacts->get(widacts, w, &a)) {
        a = alloc(vec_sigtoken_t);
        widacts->insert(widacts, w, a);

        for (size_t i=SIG_NONE; i<SIG_END; ++i)
            a->push(a, NULL);
    }
    if (a->get(a, s, &sc) && sc) {
        release(sc->val);
        release(sc);
    }
    sc = alloc(sigtoken_t);
    sc->key = s;
    sc->val = o;

    a->put(a, s, sc);
    widacts->insert(widacts, w, a);
}
#endif

static void register_action(sig s, token_t* o) {
    if (!actions)
        actions = alloc(map_sigtoken_t);
    token_t* a;
    if (actions->full(actions))
        actions->rehash(actions);
    if (!actions->get(actions, s, &a)) {
        actions->insert(actions, s, o);
        return;
    }
    release(a);
    actions->insert(actions, s, o);
}

static void unregister(token_t* o) {
    int s = 0;
    token_t* a;

    s = gsid(o->data);
    if (s) {
        if (actions && actions->get(actions, s, &a)) {
            actions->get(actions, s, &a);
            release(a);
        }
        return;
    }
    #ifdef ENABLE_X11
    if (!it)
        err(o->source, o->lineno, "error, no implied subject!\n");
    
    s = sidbe(it->etype, o->data);
    if (s) {
        vec_sigtoken_t* a;
        sigtoken_t* sc;
        if (widacts && widacts->get(widacts, it->wid, &a))
            if (a->get(a, s, &sc) && sc)
                release(sc->val);
    }
    #endif
}

static vec_token_t* interpret_block(token_t* obj, int new_scope, token_t* fname, vec_token_t* args) {
    if (!obj)
        return NULL;

    phrase_t* p = NULL;
    phrase_t* tco = NULL;
    vec_phrase_t* ps = NULL;
    vec_token_t* objs = NULL;
    token_t* t;
    token_t* o;

    ps = parse_block(obj->block->data);
    if (!ps) {
        release(obj);
        return NULL;
    }
    if (fname && ps->last(ps, &p) && 
        p->first(p, &t) &&
        kid(t->data) == CMD_RETURN &&
        p->get(p, 1, &t) &&
        eq(t->data, fname->data) &&
        p->get(p, 2, &t) &&
        t->type == T_EXPR)
    {
        debug("USING TCO\n");
        ps->pop(ps, &tco);
        tco->pop_back(tco, &t);
        release(t);
        tco->pop_back(tco, &t);
        release(t);
        tco->pop_back(tco, &t);
        release(t);
        tco->pop(tco, &t);
        release(t);
    }
    if (new_scope) {
        NEWSCOPE();
    } else { 
        scopes->push(scopes, obj->block->env);
        obj->block->env = NULL;
        scopes->last(scopes, &cs);
        debug("<BLOCK SCOPE: %p\n", (void*)cs);
    }
    release(obj);
    if (tco) {
        vec_phrase_t* tcops = alloc(vec_phrase_t);
        while (1) {
            each(ps, p, tcops->push(tcops, copy_phrase(p)););
            while (tcops->pop_back(tcops, &p)) {
                objs = parse_phrase(p, cs);
                release(p);
                while (objs && eval_expressions(objs, args) && eval_statements(objs, args) && !MUSTQUIT() && !EXITLOOP() && exec(objs));
                if (objs->count) {
                    while (ps->pop_back(ps, &p))
                        release_phrase(p);
                    while (tcops->pop_back(tcops, &p))
                        release_phrase(p);
                    release_phrase(tco);
                    release(ps);
                    release(tcops);
                    EXITSCOPE();
                    return objs; 
                }
                release_phrase(objs);
                if (MUSTQUIT() || EXITLOOP()) {
                    while (ps->pop_back(ps, &p))
                        release_phrase(p);
                    while (tcops->pop_back(tcops, &p))
                        release_phrase(p);
                    release(ps);
                    release(tcops);
                    release_phrase(tco);
                    goto end;
                }
            }
            phrase_t* tcoret = copy_phrase(tco);
            showtokens(tco);
            objs = parse_phrase(tcoret, cs);
            release(tcoret);
            eval_expressions(objs, args);
            each(args, o, release(o););
            args->clear(args);
            while (objs->pop_back(objs, &o))
                args->push(args, o);
            release(objs);
            while (tcops->pop_back(tcops, &p))
                release_phrase(p);
        }
    } else {
        while (ps->pop_back(ps, &p)) {
            objs = parse_phrase(p, cs);
            release(p);
            while (objs && eval_expressions(objs, args) && eval_statements(objs, args) && !MUSTQUIT() && !EXITLOOP() && exec(objs));
            showtokens(objs);
            if (objs->count) {
                while (ps->pop_back(ps, &p))
                    release_phrase(p);
                EXITSCOPE();
                release(ps);
                return objs;
            }
            release_phrase(objs);
            objs = NULL;
            if (MUSTQUIT() || EXITLOOP())
                break;
        }
    }
    end:
    EXITSCOPE();
    while (ps && ps->pop_back(ps, &p))
        release_phrase(p);
    release(ps);
    return objs;
}

int trigger(unsigned long w, int s) {
    token_t* c = NULL;
    if (w) {
        #ifdef ENABLE_X11
        vec_sigtoken_t* a;
        sigtoken_t* b;
        if (!widacts || !widacts->count || !widacts->get(widacts, w, &a) || !a->get(a, s, &b) || !b || !b->val)
            return 0;

        c = b->val;
        #endif
    } else {
        if (!actions || !actions->count || !actions->get(actions, s, &c))
            return 0;
    }
    switch (s) {
    case SIG_EXIT: flags &= ~EV_QUITCALLED; break;
    case SIG_TERM: sq = 0; break;
    }
    wsig = w;
    vec_token_t* ret = interpret_block(copy_token(c), 0, NULL, NULL);
    flags &= ~EV_RETURN;
    release_phrase(ret);
    wsig = 0;
    return 1;
}

void schedule() {
    if ((!schedules || !schedules->count) && (!onetimes || !onetimes->count))
        return;
    int v;
    int_timetoken_t* t;
    timetoken_t* tc;
    vec_int_t* to_del;

    if (schedules && schedules->count) {
        eachitem(schedules, int_timetoken_t, t,
            if ((time(NULL) - t->val->key) >= t->key) {
                t->val->key = time(NULL);
                vec_token_t* ret = interpret_block(copy_token(t->val->val), 0, NULL, NULL);
                flags &= ~EV_RETURN;
                release_phrase(ret);
            }
        );
    }
    if (onetimes && onetimes->count) {
        to_del = alloc(vec_int_t);
        eachitem(onetimes, int_timetoken_t, t,
            if ((time(NULL) - t->val->key) >= t->key) {
                vec_token_t* ret = interpret_block(t->val->val, 0, NULL, NULL);
                flags &= ~EV_RETURN;
                release_phrase(ret);
                to_del->push(to_del, t->key);
            }
        );
        each(to_del, v,
            if (onetimes->get(onetimes, v, &tc)) {
                release(tc);
                onetimes->remove(onetimes, v);
            }
        );
        release(to_del);
    }
}

static scope_t* checkvar(char* k) {
    scope_t* s;
    scope_t* root;
    scopes->first(scopes, &root);
    if (root->memos->contains(root->memos, k))
        return root;
    for (size_t i = 0; scopes->count && i<scopes->count-1; ++i) {
        scopes->get(scopes, scopes->count-1-i, &s);
        if (s->memos->contains(s->memos, k))
            return s;
    }
    return NULL;
}

static int isvar(char* k) {
    scope_t* s;
    token_t* r;
    for (size_t i = 0; scopes->count != i; ++i) {
        scopes->get(scopes, scopes->count-1-i, &s);
        if (s->memos->get(s->memos, k, &r) && r->type != T_BLOCK)
            return 1;
    }
    return 0;
}

static int builtin_from_var(char* k) {
    scope_t* s;
    token_t* r;
    for (size_t i = 0; scopes->count != i; ++i) {
        scopes->get(scopes, scopes->count-1-i, &s);
        if (s->memos->get(s->memos, k, &r) && r->type != T_BLOCK)
            return fid(r->data);
    }
    return 0;
}

static int isfunc(char* k) {
    scope_t* s;
    token_t* r;
    for (size_t i = 0; scopes->count != i; ++i) {
        scopes->get(scopes, scopes->count-1-i, &s);
        if (s->memos->get(s->memos, k, &r) && r->type == T_BLOCK)
            return 1;
    }
    return 0;
}

static token_t* getvar(token_t* k, vec_token_t* args) {
    token_t* r = NULL;
    if (0) {
    #ifdef ENABLE_X11
    } else if (eq(k->data, "SW") && display) {
        sprintf(buf, "%d", sw());
        r = alloc(token_t);
        r->data = salloc(buf);
        r->type = T_QUOTE;
        r->lineno = k->lineno;
        r->source = k->source;
    } else if (eq(k->data, "SH") && display) {
        sprintf(buf, "%d", sh());
        r = alloc(token_t);
        r->data = salloc(buf);
        r->type = T_QUOTE;
        r->lineno = k->lineno;
        r->source = k->source;
    } else if (eq(k->data, "X") && display) {
        intint_intint_t c = pcoords();
        sprintf(buf, "%d", c.key.key);
        r = alloc(token_t);
        r->data = salloc(buf);
        r->type = T_QUOTE;
        r->lineno = k->lineno;
        r->source = k->source;
    } else if (eq(k->data, "Y") && display) {
        intint_intint_t c = pcoords();
        sprintf(buf, "%d", c.key.val);
        r = alloc(token_t);
        r->data = salloc(buf);
        r->type = T_QUOTE;
        r->lineno = k->lineno;
        r->source = k->source;
    } else if (eq(k->data, "self") && display) {
        if (wsig) {
            r = alloc(token_t);
            sprintf(buf, "%llu", wsig);
            r->data = salloc(buf);
            r->type = T_CMD;
            r->lineno = k->lineno;
            r->source = k->source;
            return r;
        }
        return NULL;
    #endif
    } else if (eq(k->data, "FILE")) {
        r = alloc(token_t);
        r->data = (current->s == S_SOURCE) ? salloc(current->d) : salloc("");
        r->type = T_QUOTE;
        r->lineno = k->lineno;
        r->source = k->source;
    } else if (eq(k->data, "LINE")) {
        r = alloc(token_t);
        sprintf(buf, "%llu", current->lineno ? current->lineno-1 : 1);
        r->data = salloc(buf);
        r->type = T_QUOTE;
        r->lineno = k->lineno;
        r->source = k->source;
    } else if (eq(k->data, "args")) {
        token_t* t;
        r = alloc(token_t);
        r->type = T_BLOCK;
        r->lineno = k->lineno;
        r->source = k->source;
        r->block = alloc(block_t);
        r->block->env = copy_scope(cs);
        t = alloc(token_t);
        t->type = T_CODE;
        t->data = salloc("{");
        t->source = k->source;
        t->lineno = k->lineno;
        if (args) {
            r->block->data->push(r->block->data, t);
            for (size_t i=0; i<args->count; ++i) {
                args->get(args, i, &t);
                token_t* c = copy_token(t);
                c->lineno = k->lineno;
                c->source = k->source;
                if (c->type == T_BLOCK) {
                    while (c->block->data->pop_back(c->block->data, &t)) {
                        r->block->data->push(r->block->data, t);
                        t->lineno = k->lineno;
                        t->source = k->source;
                    }
                    release(c);
                } else {
                    c->lineno = k->lineno;
                    c->source = k->source;
                    r->block->data->push(r->block->data, c);
                }
            }
        }
        t = alloc(token_t);
        t->type = T_ENDCODE;
        t->data = salloc("}");
        t->source = k->source;
        t->lineno = k->lineno;
        r->block->data->push(r->block->data, t);
    } else {
        scope_t* s;
        scope_t* root;
        token_t* d = NULL;

        scopes->first(scopes, &root);
        if (root->memos->get(root->memos, k->data, &d))
            return copy_token(d);
        for (size_t i = 0; scopes->count && i<scopes->count-1; ++i) {
            scopes->get(scopes, scopes->count-1-i, &s);
            if (s->memos->get(s->memos, k->data, &d)) {
                r = copy_token(d);
                break;
            }
        }
    }
    return r;
}

static void ivar(token_t* t, vec_token_t* args, unsigned long ln, char* sn) {
    if (empty(t))
        return;
    char b[64];
    char* bp = b;
    char* s = t->data;
    char* d = NULL;
    int esc = 0;
    size_t i = 0;
    token_t* r = NULL;

    memset(buf, 0, sizeof(buf));
    while (*s) {
        if (*s == '\\' && *(s+1) && *(s+1) == '@' && *(s+2) && (*(s+2) == '{' || *(s+2) == '(')) {
            esc = 1;
            ++s;
            continue;
        }
        if (!esc && *s == '@' && *(s+1) == '{') {
            d = sadd(d, buf);
            i = 0;
            memset(buf, 0, sizeof(buf));
            s+=2;
            if (!*s) {
                d = sadd(d, "@{");
                goto end;
            }
            bp = b;
            while (*s != '}' && ((size_t)(bp - b) < (sizeof(b)-1))) {
                *bp++ = *s++;
                if (!*s) {
                    if (!*d) {
                        zfree(d);
                        d = salloc("");
                    }
                    goto end;
                }
            }
            if (*s != '}') {
                d = sadd(d, bp);
                goto end;
            }
            ++s;
            *bp = 0;
            if (args) {
                token_t* a;
                if (*b == '*') {
                    if (args && args->count) {
                        for (size_t j=0; j<args->count; ++j) {
                            if (args->get(args, j, &a)) {
                                if (a->type != T_BLOCK) {
                                    d = sadd(d, a->data);
                                } else {
                                    token_t* tt;
                                    for (size_t z=0; z<a->block->data->count; ++z) {
                                        a->block->data->get(a->block->data, z, &tt);
                                        d = sadd(d, tt->data);
                                    }
                                }
                            }
                            if (j < args->count-1)
                                d = sadd(d, " ");
                        }
                    }
                    continue;
                }
                int i = atoi(b);
                if (i) {
                    if (args->get(args, i-1, &a)) {
                        if (a->type != T_BLOCK) {
                            d = sadd(d, a->data);
                        } else {
                            token_t* tt;
                            for (size_t z=0; z<a->block->data->count; ++z) {
                                a->block->data->get(a->block->data, z, &tt);
                                d = sadd(d, tt->data);
                            }
                        }
                    }
                    continue;
                }
            }
            token_t* tmptok = alloc(token_t);
            tmptok->data = salloc(b);
            r = getvar(tmptok, args);
            release(tmptok);
            if (r) {
                r = repr(r, 0);
                d = sadd(d, r->data);
            }
            release(r);
            continue;
        } else if (!esc && *s == '@' && *(s+1) == '(') {
            d = sadd(d, buf);
            i = 0;
            memset(buf, 0, sizeof(buf));

            s+=2;
            if (!*s) {
                d = sadd(d, "@(");
                goto end;
            }
            bp = b;
            int ps = 1;
            while (ps && ((size_t)(bp - b) < (sizeof(b)-1))) {
                if (*s == '(')
                    ++ps;
                if (*s == ')')
                    --ps;
                if (!ps)
                    break;
                *bp++ = *s++;
                if (!*s) {
                    if (!*d) {
                        zfree(d);
                        d = salloc("");
                    }
                    goto end;
                }
            }
            if (*s != ')') {
                d = sadd(d, bp);
                goto end;
            }
            ++s;
            *bp = 0;

            sourcedata_t* seval = evalsrc(salloc(b), ln, salloc(sn));
            sourcedata_t* os = current;
            current = seval;

            phrase_t* p = NULL;
            vec_token_t* objs = NULL;
            while ((p = tokenize())) {
                if (!p) {
                    release(seval);
                    current = os;
                    break;
                }
                objs = parse_phrase(p, cs);
                release(p);
                if (!objs)
                    break;
                flags |= EV_RETURN;
                eval_expressions(objs, args);
                while (objs->pop_back(objs, &r)) {
                    r = repr(r, 0);
                    d = sadd(d, r->data);
                    if (objs->count)
                        d = sadd(d, " ");
                    release(r);
                }
                release(objs);
            }
            flags &= ~EV_RETURN;
            release(seval);
            current = os;
            continue;
        }
        esc = 0;
        if (i<sizeof(buf)-1) {
            buf[i++] = *s++;
        } else {
            d = sadd(d, buf);
            i = 0;
            memset(buf, 0, sizeof(buf));
        }
    }
    if (i)
        d = sadd(d, buf);
    end:
    zfree(t->data);
    t->data = d;
    return;
}

void define(token_t* var, token_t* val, scope_t* ws, vec_token_t* args) {
    char* k;
    token_t* v;
    strtoken_t* d;
    scope_t* s;
    scope_t* t;
    debug("ROOT SCOPE:%p, CURRENT SCOPE:%p\n", (void*)(scopes->first(scopes, &t), t), ((void*)cs));
    
    if (ws) {
        s = ws;
    } else {
        t = checkvar(var->data);
        s = t ? t : cs;
    }
    debug("DEFINING VAR:%s in SCOPE:%p \n", var->data, (void*)s);
    if (s->memos->item(s->memos,
        var->data, &d))
    {
        k = d->key;
        v = d->val;
        s->memos->remove(s->memos, var->data);
        zfree(k);
        release(v);
    }
    k = salloc(var->data);
    if (s->memos->full(s->memos))
        s->memos->rehash(s->memos);
    if (!val) {
        v = alloc(token_t);
        v->data = salloc("");
        v->type = T_CMD;
        v->lineno = var->lineno;
        v->source = var->source;
        s->memos->insert(s->memos, k, v);
        return;
    }
    if (val->type != T_BLOCK) {
        if (val->type == T_CMD) {
            if (isfunc(val->data)) {
                token_t* r = getvar(val, args);
                s->memos->insert(s->memos, k, r);
                return;
            }
        }
        v = copy_token(val);
        s->memos->insert(s->memos, k, v);
        return;
    }
    s->memos->insert(s->memos, k, copy_token(val));
}

static void varsub(token_t* ft, vec_token_t* v, vec_token_t* args) {
    int idx = 0;
    token_t* o = NULL;
    if (!v->pop_back(v, &o))
        err(ft->source, ft->lineno, "error, bad dereference.\n");
    if (o->type != T_CMD && o->type != T_QUOTE && o->type != T_DQUOTE)
        err(ft->source, ft->lineno, "error, bad dereference.\n");
    idx = atoi(o->data);
    if (idx) { 
        token_t* a;
        if (args && args->get(args, idx-1, &a)) {
            v->push_back(v, copy_token(a));
            release(o);
            return;
        }
        release(o);
        return;
    }
    token_t* r = getvar(o, args);
    if (r && (r->type != T_BLOCK)) {
        if (*r->data) {
            r->lineno = o->lineno;
            r->source = o->source;
            release(o);
            v->push_back(v, r);
            return;
        }
        release(o);
        release(r);
        return;
    }
    if (r) {
        token_t* tok;
        each(r->block->data, tok,
            tok->lineno = o->lineno;
            tok->source = o->source;
        );
        release(o);
        v->push_back(v, r);
        return;
    }
    release(o);
}

size_t eval_expressions(vec_token_t* v, vec_token_t* args) {
    if (!v->count)
        return 0;
    debug("EVAL CALLED\n");
    showtokens(v);

    token_t* a = NULL;
    token_t* o = NULL;
    token_t* peek = NULL;
    token_t* func = NULL;
    token_t* t = NULL;

    vec_exprblock_t* blocks = NULL;
    exprblock_t* eb = NULL;
    vec_token_t* params = NULL;
    vec_token_t* s = NULL;

    unsigned long ln;
    char* sn;
    double r = 0;
    double r2 = 0;
    int expr = 0;
    int skipexpr = 0;
    int f;

    #ifdef ENABLE_X11
    #define checkwid_and_get(T)                                                      \
        widget_t* w = NULL;                                                          \
        do {                                                                         \
            unsigned long wid = strtoul(T->data, NULL, 10);                          \
            if (!widgets || !wid || !widgets->get(widgets, wid, &w))                 \
                err(T->source, T->lineno, "error, no such widget '%s'!\n", T->data); \
        } while (0)
    #endif

    s = alloc(vec_token_t);
    expr = 0;
    skipexpr = 0;
    while (v->pop_back(v, &o)) {
        if (o->type != T_BLOCK) {
            ln = o->lineno;
            sn = o->source;
            switch (o->type) {
            case T_SLICE: {
                release(o);
                int atype = T_BLOCK;
                if (s->last(s, &o) && o->type != T_BLOCK)
                    atype = T_CMD;
                int slice = 1;
                vec_token_t* slo = alloc(vec_token_t);
                while (v->pop_back(v, &o)) {
                    if (o->type == T_SLICE)
                        ++slice;
                    else if (o->type == T_ENDSLICE)
                        --slice;
                    if (!slice) {
                        release(o);
                        break;
                    }
                    slo->push(slo, o);
                }
                vec_token_t* objs = parse_phrase(slo, cs);
                release(slo);
                eval_expressions(objs, args);
                if (!objs->count)
                    err(sn, ln, "error, slice expression requires at least an index.\n");
                objs->pop_back(objs, &o);
                if (o->type == T_BLOCK)
                    err(sn, ln, "error, block given instead of an index.\n");

                long si = tolong(o);
                long ei = si;
                release(o);

                if (objs->pop_back(objs, &o)) {
                    if (o->type == T_BLOCK)
                        err(sn, ln, "error, block given instead of an index.\n");
                    ei = tolong(o);
                    release(o);
                }
                release_phrase(objs);

                if (atype == T_BLOCK) {
                    if (!s->pop(s, &o))
                        err(sn, ln, "error, slice expression requires an object.\n");
                    showtokens(o->block->data);
                    decap_block(o->block->data);
                    vec_token_t* bobjs = parse_phrase(o->block->data, cs);
                    release(o);
                    showtokens(bobjs);
                    if (bobjs->count && si <= ei && si >= 0 && si <= ((long)bobjs->count-1)) {
                        ei = ei > ((long)bobjs->count-1) ? (long)bobjs->count-1 : ei;
                        for (long j=ei; j>=si; --j) {
                            bobjs->get(bobjs, j, &o);
                            v->push_back(v, copy_token(o));
                        }
                    } else {
                        o = alloc(token_t);
                        o->type = T_QUOTE;
                        o->data = salloc("");
                        o->lineno = ln;
                        o->source = sn;
                        v->push_back(v, o);
                    }
                    release_phrase(bobjs);
                    continue;
                }
                s->pop(s, &o);
                size_t count = strlen(o->data)+1;
                token_t* nt = alloc(token_t);
                nt->type = o->type;
                nt->lineno = o->lineno;
                nt->source = o->source;
                if (count && si <= ei && si >= 0 && si <= ((long)count-1)) {
                    ei = ei > ((long)count-1) ? (long)count-1 : ei;
                    nt->data = calloc(1, sizeof(char)*(ei-si+2));
                    long z = 0;
                    for (long j=si; j<=ei; ++j)
                        nt->data[z++] = o->data[j];
                } else {
                    nt->type = T_QUOTE;
                    nt->data = salloc("");
                }
                release(o);
                o = nt;
                v->push_back(v, o);
                continue;
            }
            case T_EXPR:
                ++expr;
                s->push(s, o);
                if (v->first(v, &peek)) { 
                    if (peek->type == T_EXPR) {
                        ++expr;
                        skipexpr = 1;
                        v->pop_back(v, &peek);
                        release(peek);
                        continue;
                    }
                    peek = NULL;
                }
                skipexpr = 0;
                continue;
            case T_ENDEXPR:
                --expr;
                if (skipexpr) {
                    skipexpr = 0;
                    release(o);
                    continue;
                }
                s->push(s, o);
                break;
            case T_DQUOTE:
                ivar(o, args, ln, sn);
                s->push(s, o);
                continue;
            case T_VARSUB:
                varsub(o, v, args);
                release(o);
                continue;
            case T_ARGSSUB: {
                release(o);
                if (args && args->count)
                    for (long j=((long)args->count-1); j>=0; --j)
                        if (args->get(args, j, &o))
                            v->push_back(v, copy_token(o));
                continue;
            }
            case T_RANGE: {
                token_t* start;
                token_t* end;

                if (!v->pop_back(v, &end))
                    err(o->source, o->lineno, "error, end of range is needed.\n");

                assert_notblock(end, "end", o->data);
                if (!s->pop(s, &start))
                    err(o->source, o->lineno, "error, start of range is needed.\n");

                assert_notblock(start, "start", o->data);
                long st = tolong(start);
                long en = tolong(end);

                release(start);
                release(end);
                release(o);

                if (st > en)
                    err(sn, ln, "error, 'range' start number must be greater than end one.\n");
                for (long j=st; j<=en; ++j) {
                    t = alloc(token_t);
                    sprintf(buf, "%ld", j);
                    t->data = salloc(buf);
                    t->type = T_QUOTE;
                    t->lineno = ln;
                    t->source = sn;
                    s->push(s, t);
                }
                continue;
            }
            #ifdef ENABLE_X11
            case T_ATTR: {
                if (!display)
                    err(sn, ln, "error, no display!\n");
                token_t* prev;
                token_t* attr;

                if (!v->pop_back(v, &attr))
                    err(o->source, o->lineno, "error, an attribute is needed.\n");

                assert_notblock(attr, "main", o->data);
                int wa = aid(attr->data);
                widget_t* w;
                if (s->last(s, &prev)) {
                    unsigned long wid = 0;
                    errno = 0;
                    wid = strtoul(prev->data, NULL, 10);
                    if (errno != EINVAL && errno != ERANGE && wid) {
                        if (!widgets->get(widgets, wid, &w))
                            err(prev->source, prev->lineno, "error, unknown window id '%lu'.\n", wid);
                        s->pop(s, &prev);
                        release(prev);
                    } else {
                        if (!it)
                            err(o->source, o->lineno, "error, no implied subject!\n");
                        w = it;
                    }
                }
                release(o);
                switch (wa) {
                case ATTR_PID:
                    zfree(attr->data);
                    sprintf(buf, "%d", w->pid);
                    attr->data = salloc(buf);
                    break;
                case ATTR_W:
                    zfree(attr->data);
                    sprintf(buf, "%d", w->w);
                    attr->data = salloc(buf);
                    break;
                case ATTR_H:
                    zfree(attr->data);
                    sprintf(buf, "%d", w->h);
                    attr->data = salloc(buf);
                    break;
                case ATTR_X:
                    zfree(attr->data);
                    sprintf(buf, "%d", w->x);
                    attr->data = salloc(buf);
                    break;
                case ATTR_Y:
                    zfree(attr->data);
                    sprintf(buf, "%d", w->y);
                    attr->data = salloc(buf);
                    break;
                case ATTR_B:
                    zfree(attr->data);
                    sprintf(buf, "%d", w->b);
                    attr->data = salloc(buf);
                    break;
                case ATTR_G:
                    zfree(attr->data);
                    sprintf(buf, "%d", w->m);
                    attr->data = salloc(buf);
                    break;
                case ATTR_D:
                    zfree(attr->data);
                    attr->data = salloc(w->data);
                    break;
                case ATTR_C:
                    zfree(attr->data);
                    sprintf(buf, "%d", !!(w->flags & F_CHECKED));
                    attr->data = salloc(buf);
                    break;
                case ATTR_V:
                    zfree(attr->data);
                    sprintf(buf, "%d", !!(w->flags & F_VISIBLE));
                    attr->data = salloc(buf);
                    break;
                case ATTR_E:
                    zfree(attr->data);
                    sprintf(buf, "%d", !(w->flags & F_FREEZED));
                    attr->data = salloc(buf);
                    break;
                case ATTR_F:
                    zfree(attr->data);
                    sprintf(buf, "%d", !(w->flags & F_FOCUSED));
                    attr->data = salloc(buf);
                    break;
                case ATTR_T:
                    zfree(attr->data);
                    attr->data = salloc((char*)ename(w->etype));
                    break;
                case ATTR_TL:
                    zfree(attr->data);
                    attr->data = salloc(w->title);
                    break;
                case ATTR_S: {
                    page_t* p = (page_t*)w;
                    if (w->etype != E_PAGE || !p->subs->count) {
                        zfree(attr->data);
                        attr->data = salloc("");
                        attr->type = T_QUOTE;
                        break;
                    }
                    release(attr);
                    widget_t* c;
                    each(p->subs, c,
                        attr = alloc(token_t);
                        sprintf(buf, "%lu", c->wid);
                        attr->data = salloc(buf);
                        attr->type = T_CMD;
                        attr->lineno = ln;
                        attr->source = sn;
                        s->push(s, attr);
                    );
                    continue;
                }
                case ATTR_N: {
                    page_t* p = (page_t*)w;
                    if (w->etype != E_PAGE || !p->subs->count) {
                        zfree(attr->data);
                        attr->data = salloc("0");
                        attr->type = T_QUOTE;
                        break;
                    }
                    sprintf(buf, "%lu", (unsigned long)p->subs->count);
                    zfree(attr->data);
                    attr->data = salloc(buf);
                    break;
                }
                default: {
                    scope_t* ws;
                    token_t* d;
                    if (wscopes && wscopes->get(wscopes, w->wid, &ws)) {
                        if (ws->memos->get(ws->memos, attr->data, &d)) {
                            release(attr);
                            attr = copy_token(d);
                            break;
                        }
                    }
                    err(attr->source, attr->lineno, "error, no such attribute '%s'.\n", attr->data);
                }
                }
                attr->lineno = ln;
                v->push_back(v, attr);
                continue;
            }
            case T_GLOB: {
                if (!display)
                    err(sn, ln, "error, no display!\n");
                release(o);

                if (!widgets || !widgets->count)
                    continue;

                widwidget_t* ww = NULL;
                eachitem(widgets, widwidget_t, ww,
                    sprintf(buf, "%lu", ww->key);
                    o = alloc(token_t);
                    o->data = salloc(buf);
                    o->type = T_CMD;
                    o->lineno = ln;
                    o->source = sn;
                    v->push_back(v, o);
                );
                o = NULL;
                continue;
            }
            case T_ELEM: {
                if (!display)
                    err(sn, ln, "error, no display!\n");
                release(o);
                vec_token_t* slo = alloc(vec_token_t);
                while (v->pop_back(v, &o)) {
                    if (o->type == T_ELEM) {
                        release(o);
                        break;
                    }
                    slo->push(slo, o);
                }
                vec_token_t* objs = parse_phrase(slo, cs);
                release(slo);
                eval_expressions(objs, args);
                if (!objs->count)
                    err(sn, ln, "error, empty element expression!\n");
                while (objs->pop_back(objs, &o)) {
                    int el;
                    int ww = 0;
                    int hh = 0;
                    if ((el = eid(o->data))) {
                        token_t* a;
                        token_t* attrs;
                        if (objs->first(objs, &attrs) && attrs->type == T_BLOCK) {
                            objs->pop_back(objs, &attrs);
                            decap_block(attrs->block->data);
                            vec_token_t* sobjs = parse_phrase(attrs->block->data, cs);
                            eval_expressions(sobjs, args);
                            if (sobjs->first(sobjs, &a)) {
                                assert_notblock(a, "width", o->data);
                                ww = tolong(a);
                            }
                            if (sobjs->get(sobjs, 1, &a)) {
                                assert_notblock(a, "height", o->data);
                                hh = tolong(a);
                            }
                            release_phrase(sobjs);
                            release(attrs);
                        }
                        it = make(el, 0, ww, hh);
                        if (!todel)
                            todel = alloc(vec_wid_t);
                        sprintf(buf, "%lu", it->wid);
                        zfree(o->data);
                        o->data = salloc(buf);
                        o->type = T_CMD;
                        s->push(s, o);
                        continue;
                    }
                    char* e = NULL;
                    errno = 0;
                    Window w = strtoul(o->data, &e, 10);
                    if (*e || errno == ERANGE || errno == EINVAL)
                        err(sn, ln, "error, unknown element '%s'.\n", o->data);
                    if (!iswin(root, w))
                        err(sn, ln, "error, no existing window with id '%s'.\n", o->data);
                    it = make(0, w, 0, 0);
                    if (!todel)
                        todel = alloc(vec_wid_t);
                    sprintf(buf, "%lu", w);
                    zfree(o->data);
                    o->data = salloc(buf);
                    o->type = T_CMD;
                    s->push(s, o);
                }
                release_phrase(objs);
                continue;
            }
            case T_WIDSUB: {
                if (!display)
                    err(sn, ln, "error, no display!\n");
                ivar(o, args, ln, sn);
                r = fork();
                if (r < 0)
                    err(sn, ln, "error, unable to fork new process.\n");
                if (r) {
                    Window w = 0;
                    char* mw = getenv("GUISH_MAXWIDWAIT");
                    long c = mw ? atoi(mw) : MAXWIDWAIT;
                    struct timeval b;
                    struct timeval a;

                    memset(&b, 0, sizeof(b));
                    gettimeofday(&b, NULL);
                    while (!(w = widpid(root, r))) {
                        memset(&a, 0, sizeof(a));
                        gettimeofday(&a, NULL);

                        if (a.tv_sec - b.tv_sec > c)
                            err(sn, ln, "error, cannot find a window id for '%s'.\n", o->data);
                        usleep(1000);
                    }
                    sprintf(buf, "%lu", w);
                    free(o->data);
                    o->data = salloc(buf);
                    o->type = T_CMD;
                    it = make(0, w, 0, 0);
                    if (!todel)
                        todel = alloc(vec_wid_t);
                    exts->insert(exts, r, it);
                } else {
                    vec_str_t* a = alloc(vec_str_t);
                    ssplit(o->data, ' ', a, NULL);
                    char* args[BSIZ] = {0};
                    char* p;
                    each(a, p,
                        args[__i] = p;
                        debug("%s ", p);
                    );
                    debug("\n");
                    if (execvp(args[0], args) < 0)
                        exit(EXIT_FAILURE);
                    each(a, p,
                        zfree(p);
                    );
                    release(a);
                }
                v->push_back(v, o);
                continue;
            }
            #endif
            case T_SHELLSUB: {
                ivar(o, args, ln, sn);
                char* sub = shellsub(NULL, o->data);
                if (!sub) {
                    release(o);
                    continue;
                }
                free(o->data);
                trim(sub);
                o->type = T_CMD;
                o->data = sub;
                v->push_back(v, o);
                continue;
            }
            default: {
                s->push(s, o);
                continue;
            }
            }
        } else {
            if (v->first(v, &peek) && peek->type == T_EXPR) {
                if (!blocks)
                    blocks = alloc(vec_exprblock_t);
                eb = alloc(exprblock_t);
                eb->key = expr;
                eb->val = o;
                blocks->push(blocks, eb);
                peek = NULL;
            } else {
                s->push(s, o);
            }
            continue;
        }
        params = alloc(vec_token_t);
        showtokens(s);
        s->pop(s, &a);
        release(a);
        while (s->pop(s, &a) && a->type != T_EXPR)
            params->push_back(params, a);
        release(a);
        #ifdef DEBUG
        if (blocks && blocks->last(blocks, &eb))
            debug("LAST BLOCK EXPR: %d, CURRENT EXPR: %d\n", eb->key, expr);
        #endif
        if (blocks && blocks->last(blocks, &eb) && eb->key == expr) {
            blocks->pop(blocks, &eb);
            vec_token_t* ret = interpret_block(eb->val, 0, NULL, params);
            flags &= ~EV_RETURN;
            release(eb);
            while (ret && ret->pop(ret, &a))
                v->push_back(v, a);
            release(func);
            release(ret);
            release_phrase(params);
            continue;
        }
        if (!s->pop(s, &func)) {
            release_phrase(params);
            debug("GOTO push_args\n");
            goto push_args;
        }
        #ifdef DEBUG
        debug("APPLYING FUNC: '%s'\n", func->data);
        debug("NUMBER OF PARAMETERS : %lu\n", (unsigned long)params->count);
        debug("------------------------\n");
        debug("FUNCTION PARAMETERS:\n");
        showtokens(params);
        debug("------------------------\n");
        #endif
        if (func->type == T_BLOCK || func->type != T_CMD)
            err(func->source, func->lineno, "error, '%s' is not a function!\n", func->data);
        
        f = builtin_from_var(func->data);
        f = f ? f : isfunc(func->data) ? 0 : fid(func->data);
        
        callbuiltin:
        if (f) {
            long nargs = fargs(f);
            if ((!nargs && params->count) ||
                (nargs > 0 && ((long)params->count != nargs)) ||
                (nargs < -1 && ((long)params->count < (labs(nargs)-1))))
            {
                err(sn, ln, "error, wrong number of arguments for function '%s'.\n", func->data);
            }
        }
        switch (f) {
            #ifdef ENABLE_X11
            case F_EXISTS: {
                if (!display)
                    err(sn, ln, "error, no display!\n");
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                sprintf(buf, "%d", (widgets && widgets->contains(widgets, strtoul(arg->data, NULL, 10))));
                release(func);
                zfree(arg->data);
                arg->data = salloc(buf);
                arg->type = T_QUOTE;
                s->push(s, arg);
                break;
            }
            #endif
            case F_IF:
            case F_UNLESS: {
                token_t* cond;
                token_t* ifblock = NULL;
                token_t* elblock = NULL;
                int istrue;

                if (params->count == 1) {
                    s->push(s, func);
                    params->pop_back(params, &cond);
                    s->push(s, cond);
                    break;
                }
                params->pop_back(params, &cond);
                params->pop_back(params, &ifblock);

                release(func);
                istrue = true(cond);
                if ((f == F_IF && !istrue) || (f == F_UNLESS && istrue)) {
                    release(ifblock);
                    release(cond);
                    if (params->pop_back(params, &elblock))
                        v->push_back(v, elblock);
                    break;
                }
                v->push_back(v, ifblock);
                release(cond);
                break;
            }
            case F_AND: {
                token_t* arg;
                token_t* t;
                while (params->pop_back(params, &arg)) {
                    if (arg->type == T_BLOCK) {
                        decap_block(arg->block->data);
                        vec_token_t* objs = parse_phrase(arg->block->data, cs);
                        if (!eval_expressions(objs, NULL)) {
                            release(arg);
                            release_phrase(objs);
                            break;
                        }
                        int istrue = 1;
                        for (size_t i=0; i<objs->count; ++i) {
                            objs->get(objs, i, &t);
                            if (!true(t)) {
                                istrue = 0;
                                break;
                            }
                        }
                        if (!istrue || !params->count) {
                            while (objs->pop(objs, &t))
                                v->push_back(v, t);
                            release(arg);
                            release_phrase(objs);
                            break;
                        } 
                        release(arg);
                        release_phrase(objs);
                    } else {
                        if (!true(arg) || !params->count) {
                            v->push_back(v, arg);
                            break;
                        }
                        release(arg);
                    }
                }
                release(func);
                break;
            }
            case F_OR: {
                token_t* arg;
                token_t* t;
                while (params->pop_back(params, &arg)) {
                    if (arg->type == T_BLOCK) {
                        decap_block(arg->block->data);
                        vec_token_t* objs = parse_phrase(arg->block->data, cs);
                        if (!eval_expressions(objs, NULL)) {
                            release(arg);
                            release_phrase(objs);
                            continue;
                        }
                        int istrue = 1;
                        for (size_t i=0; i<objs->count; ++i) {
                            objs->get(objs, i, &t);
                            if (!true(t)) {
                                istrue = 0;
                                break;
                            }
                        }
                        if (istrue || !params->count) {
                            while (objs->pop(objs, &t))
                                v->push_back(v, t);
                            release(arg);
                            release_phrase(objs);
                            break;
                        } 
                        release(arg);
                        release_phrase(objs);
                    } else {
                        if (true(arg) || !params->count) {
                            v->push_back(v, arg);
                            break;
                        }
                        release(arg);
                    }
                }
                release(func);
                break;
            }
            case F_LET: {
                token_t* var = NULL;
                token_t* val = NULL;
                while (params->count) {
                    params->pop_back(params, &var);
                    assert_notblock(var, "var", func->data);
                    params->pop_back(params, &val);
                    define(var, val, NULL, args);
                    release(var);
                    release(val);
                }
                release(func);
                break;
            }
            case F_SOME: {
                token_t* arg;
                zfree(func->data);
                if (!params->count) {
                    func->data = salloc("");
                    s->push(s, func);
                    break;
                }
                release(func);
                while (params->pop_back(params, &arg))
                    s->push(s, arg);
                break;
            }
            case F_TRUE:
            case F_FALSE: {
                token_t* arg;
                params->pop_back(params, &arg);
                zfree(func->data);
                func->data = salloc(f == F_TRUE ? (true(arg) ? "1" : "0") : (true(arg) ? "0" : "1"));
                s->push(s, func);
                release(arg);
                break;
            }
            case F_PUSH: {
                token_t* arg;
                release(func);
                if (!args)
                    break;
                while (params->pop_back(params, &arg))
                    args->push(args, arg);
                break;
            }
            case F_PUSHB: {
                token_t* arg;
                release(func);
                if (!args)
                    break;
                while (params->pop_back(params, &arg))
                    args->push_back(args, arg);
                break;
            }
            case F_POP: {
                token_t* arg;
                release(func);
                if (!args)
                    break;
                if (args->pop(args, &arg))
                    s->push(s, arg);
                break;
            }
            case F_POPB: {
                token_t* arg;
                release(func);
                if (!args)
                    break;
                if (args->pop_back(args, &arg))
                    s->push(s, arg);
                break;
            }
            case F_PUTS: {
                token_t* arg;
                release(func);
                while (params->pop_back(params, &arg)) {
                    arg = repr(arg, 0);
                    printf("%s", arg->data);
                    release(arg);
                    if (params->count)
                        printf(" ");
                }
                printf("\n");
                break;
            }
            case F_GET: {
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                int idx = atoi(arg->data);
                release(func);
                if (args && idx) {
                    release(arg);
                    if (args->get(args, idx-1, &arg)) {
                        s->push(s, copy_token(arg));
                    } else {
                        arg = alloc(token_t);
                        arg->data = salloc("");
                        arg->lineno = ln;
                        arg->source = sn;
                        s->push(s, arg);
                    }
                    break;
                }
                token_t* r = getvar(arg, args);
                if (r && r->type == T_BLOCK && r->block->data->count) {
                    v->push_back(v, r);
                    release(arg);
                    break;
                }
                if (r && (r->type != T_BLOCK || builtin_from_var(r->data))) {
                    release(arg);
                    s->push(s, r);
                    break;
                }
                zfree(arg->data);
                arg->data = salloc("");
                arg->lineno = ln;
                s->push(s, arg);
                break;
            }
            case F_REV: {
                token_t* arg;
                release(func);
                params->last(params, &arg);
                if (arg->type == T_BLOCK) {
                    params->pop(params, &arg);
                    token_t* end1;
                    token_t* end2;
                    arg->block->data->pop_back(arg->block->data, &end1);
                    arg->block->data->pop(arg->block->data, &end2);
                    vlrev(token_t*, arg->block->data);
                    arg->block->data->push_back(arg->block->data, end1);
                    arg->block->data->push(arg->block->data, end2);
                    s->push(s, arg);
                } else {
                    while (params->pop(params, &arg))
                        s->push(s, arg);
                }
                break;
            }
            case F_IN: {
                token_t* op1;
                token_t* op2;

                params->pop_back(params, &op1);
                params->pop_back(params, &op2);

                token_t* rop1 = repr(op1, 0);
                token_t* rop2 = repr(op2, op2->type == T_BLOCK ? 1 : 0);

                func->type = T_QUOTE;
                zfree(func->data);
                sprintf(buf, "%d", strstr(rop2->data, rop1->data) != NULL);
                func->data = salloc(buf);
                release(rop1);
                release(rop2);
                s->push(s, func);
                break;
            }
            case F_JOIN: {
                token_t* res;
                token_t* arg;
                token_t* t;
                token_t* end;
                if (!params->count) {
                    release(func);
                    break;
                }
                params->pop_back(params, &res);
                if (!params->count) {
                    v->push_back(v, res);
                    release(func);
                    break;
                }
                while (params->count) {
                    params->pop_back(params, &arg);
                    if (res->type == T_BLOCK) {
                        res->block->data->pop(res->block->data, &end);
                        if (arg->type == T_BLOCK) {
                            release(end);
                            arg->block->data->pop_back(arg->block->data, &end);
                            release(end);
                            arg->block->data->pop(arg->block->data, &end);
                            while (arg->block->data->pop_back(arg->block->data, &t))
                                res->block->data->push(res->block->data, t);
                            res->block->data->push(res->block->data, end);
                        } else {
                            res->block->data->push(res->block->data, arg);
                            arg->lineno = end->lineno;
                            res->block->data->push(res->block->data, end);
                            arg = NULL;
                        }
                        release(arg);
                    } else {
                        if (arg->type == T_BLOCK) {
                            arg->block->data->pop_back(arg->block->data, &end);
                            res->lineno = end->lineno;
                            arg->block->data->push_back(arg->block->data, res);
                            arg->block->data->push_back(arg->block->data, end);
                        } else {
                            res->data = sadd(res->data, arg->data);
                            zfree(arg->data);
                            arg->data = res->data;
                            res->data = NULL;
                            release(res);
                        }
                        res = arg;
                    }
                }
                v->push_back(v, res);
                release(func);
                break;
            }
            case F_TIMES: {
                token_t* n;
                token_t* arg;

                params->pop_back(params, &n);
                assert_notblock(n, "first", func->data);
                r = tolong(n);
                release(n);
                release(func);
                params->pop_back(params, &arg);
                for (long j=0; j<r; ++j)
                    s->push(s, copy_token(arg));
                release(arg);
                break;
            }
            case F_ISDEF:
            case F_ISVAR:
            case F_ISFUNC: {
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                int ret = f == F_ISDEF ? (checkvar(arg->data) != NULL) :
                          f == F_ISVAR ? isvar(arg->data) :
                          f == F_ISFUNC ? (isfunc(arg->data) || fid(arg->data)) :
                          0;
                sprintf(buf, "%d", ret);
                func->type = T_QUOTE;
                release(arg);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_ISBLOCK: {
                token_t* arg;
                if (!params->count || params->count > 1) {
                    zfree(func->data);
                    func->data = salloc("0");
                    func->type = T_QUOTE;
                    s->push(s, func);
                    break;
                }
                params->pop_back(params, &arg);
                zfree(func->data);
                sprintf(buf, "%d", (arg->type == T_BLOCK));
                release(arg);
                func->type = T_QUOTE;
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_ISINT: {
                token_t* arg;
                if (!params->count || params->count > 1) {
                    zfree(func->data);
                    func->data = salloc("0");
                    func->type = T_QUOTE;
                    s->push(s, func);
                    break;
                }
                params->pop_back(params, &arg);
                zfree(func->data);
                r = 0;
                if (arg->type != T_BLOCK) {
                    char* e = NULL;
                    strtol(arg->data, &e, 10);
                    r = (*e || errno == EINVAL) ? 0 : 1;
                }
                release(arg);
                sprintf(buf, "%d", !!r);
                func->type = T_QUOTE;
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_LEN: {
                token_t* arg;
                params->pop_back(params, &arg);
                if (arg->type != T_BLOCK) {
                    sprintf(buf, "%lu", (unsigned long)strlen(arg->data));
                } else {
                    decap_block(arg->block->data);
                    vec_token_t* objs = parse_phrase(arg->block->data, cs);
                    sprintf(buf, "%lu", (unsigned long)objs->count);
                    release_phrase(objs);
                }
                func->type = T_QUOTE;
                release(arg);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_SPLIT:
            case F_CSPLIT: {
                token_t* arg;
                token_t* sep;
                char* d;
                int type;
                unsigned long long line;

                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                params->pop_back(params, &sep);
                assert_notblock(arg, "second", func->data);

                type = f == F_SPLIT ? arg->type : T_CMD;
                release(func);

                vec_str_t* l = alloc(vec_str_t);
                ssplit(arg->data, *sep->data, l, NULL);
                line = arg->lineno;
                sn = arg->source;
                release(arg);
                release(sep);
                while (l->count) {
                    l->pop_back(l, &d);
                    arg = alloc(token_t);
                    arg->lineno = line;
                    arg->source = sn;
                    arg->type = type;
                    arg->data = d;
                    s->push(s, arg);
                }
                release(l);
                break;
            }
            case F_ABS:
            case F_NEG:
            case F_NOT: {
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);

                switch (f) {
                case F_ABS: sprintf(buf, "%lf", fabs(todouble(arg))); break;
                case F_NEG: sprintf(buf, "%lf", -todouble(arg)); break;
                case F_NOT: sprintf(buf, "%d", !tolong(arg)); break;
                }
                release(arg);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_ADD: {
                r = 0;
                r2 = 0;
                token_t* arg;
                while (params->pop_back(params, &arg)) {
                    assert_notblock(arg, "", func->data);
                    r2 = todouble(arg);
                    release(arg);
                    r += r2;
                }
                sprintf(buf, (floor(r) == ceil(r)) ? "%.0f" : "%g", r);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_SUB: {
                token_t* arg;
                if (!params->count) {
                    r = 0;
                } else {
                    params->pop_back(params, &arg);
                    assert_notblock(arg, "", func->data);
                    r = todouble(arg);
                    release(arg);
                    while (params->pop_back(params, &arg)) {
                        assert_notblock(arg, "", func->data);
                        r2 = todouble(arg);
                        release(arg);
                        r -= r2;
                    }
                }
                sprintf(buf, (floor(r) == ceil(r)) ? "%.0f" : "%g", r);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_MUL:
            case F_DIV:
            case F_MOD: {
                token_t* arg;
                if (!params->count) {
                    r = 0;
                } else {
                    switch (f) {
                        case F_MUL:
                            r = 1;
                            while (params->pop_back(params, &arg)) {
                                assert_notblock(arg, "", func->data);
                                r2 = todouble(arg);
                                release(arg);
                                r *= r2;
                            }
                            break;
                        case F_DIV:
                        case F_MOD: {
                            r = 0;
                            params->pop_back(params, &arg);
                            assert_notblock(arg, "", func->data);
                            r = todouble(arg);
                            release(arg);
                            while (params->pop_back(params, &arg)) {
                                assert_notblock(arg, "", func->data);
                                double n = todouble(arg);
                                release(arg);
                                if (!n)
                                    err(func->source, func->lineno, "error, division by zero.\n");
                                switch (f) {
                                    case F_DIV: r /= n; break;
                                    case F_MOD: r = (long)r % (long)n; break;
                                }
                            }
                            break;
                        }
                    }
                }
                sprintf(buf, (floor(r) == ceil(r)) ? "%.0f" : "%g", r);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_RAND: {
                r = 0;
                struct timeval tw;
                memset(&tw, 0, sizeof(tw));
                gettimeofday(&tw, NULL);
                srand(tw.tv_usec);
                sprintf(buf, "%u", (unsigned)rand());
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_SQRT:
            case F_CBRT:
            case F_SIN:
            case F_COS:
            case F_TAN: 
            case F_LOG: 
            case F_LN: {
                r = 0;
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                r2 = todouble(arg);
                release(arg);

                switch (f) {
                case F_SQRT: r = sqrt(r2); break;
                case F_CBRT: r = cbrt(r2); break;
                case F_SIN: r = sin(r2*((atan(1)*4)/180)); break;
                case F_COS: r = cos(r2*((atan(1)*4)/180)); break;
                case F_TAN: r = tan(r2*((atan(1)*4)/180)); break;
                case F_LOG: r = log10(r2); break;
                case F_LN: r = log(r2); break;
                }
                sprintf(buf, (floor(r) == ceil(r)) ? "%.0f" : "%f", r);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_POW: {
                r = 0;
                token_t* f1;
                token_t* f2;
                params->pop_back(params, &f1);
                assert_notblock(f1, "first", func->data);
                params->pop_back(params, &f2);
                assert_notblock(f2, "second", func->data);
                r = pow(todouble(f1), todouble(f2));
                release(f1);
                release(f2);
                sprintf(buf, (floor(r) == ceil(r)) ? "%.0f" : "%f", r);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_HEX:
            case F_INT: {
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                r = todouble(arg);
                release(arg);
                switch (f) {
                    case F_HEX: sprintf(buf, "%x", (unsigned)r); break;
                    case F_INT: sprintf(buf, "%.0f", round(r)); break;
                }
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_XOR:
            case F_BAND:
            case F_BOR:
            case F_LSHIFT:
            case F_RSHIFT: {
                long r = 0;
                long g;
                long n;
                token_t* op1;
                token_t* op2;
                params->pop_back(params, &op1);
                assert_notblock(op1, "first", func->data);
                g = tolong(op1);
                release(op1);
                params->pop_back(params, &op2);
                assert_notblock(op2, "second", func->data);
                n = tolong(op2);
                release(op2);

                switch (f) {
                    case F_LSHIFT:
                        r = g << n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            n = tolong(op1);
                            release(op1);
                            r <<= n;
                        }
                        break;
                    case F_RSHIFT:
                        r = g >> n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            n = tolong(op1);
                            release(op1);
                            r >>= n;
                        }
                        break;
                    case F_XOR:
                        r = g ^ n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            n = tolong(op1);
                            release(op1);
                            r ^= n;
                        }
                        break;
                    case F_BAND:
                        r = g & n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            n = tolong(op1);
                            release(op1);
                            r &= n;
                        }
                        break;
                    case F_BOR:
                        r = g | n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            n = tolong(op1);
                            release(op1);
                            r |= n;
                        }
                        break;
                }
                sprintf(buf, "%ld", r);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_SEQ: {
                char* e = NULL;
                token_t* op1;
                token_t* op2;

                params->pop_back(params, &op1);
                assert_notblock(op1, "first", func->data);
                params->pop_back(params, &op2);
                assert_notblock(op2, "second", func->data);

                e = eq(op1->data, op2->data) ? op2->data : NULL;
                release(op1);
                while (params->pop_back(params, &op1)) {
                    assert_notblock(op1, "", func->data);
                    if (e)
                        e = eq(op1->data, op2->data) ? op2->data : NULL;
                    release(op1);
                }
                release(op2);
                sprintf(buf, "%d", e != NULL);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_EQ:
            case F_NE:
            case F_GT:
            case F_GE:
            case F_LT:
            case F_LE: {
                double g;
                double n;
                token_t* op1;
                token_t* op2;

                params->pop_back(params, &op1);
                assert_notblock(op1, "first", func->data);
                params->pop_back(params, &op2);
                assert_notblock(op2, "second", func->data);

                g = todouble(op1);
                release(op1);

                n = todouble(op2);
                release(op2);

                switch (f) {
                    case F_EQ:
                        r = g == n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            if (r) {
                                g = todouble(op1);
                                release(op1);
                                r = n == g;
                                n = g;
                            }
                            release(op1);
                        }
                        break;
                    case F_NE:
                        r = g != n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            if (r) {
                                g = todouble(op1);
                                release(op1);
                                r = n != g;
                                n = g;
                            }
                            release(op1);
                        }
                        break;
                    case F_LT:
                        r = g < n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            if (r) {
                                g = todouble(op1);
                                release(op1);
                                r = n < g;
                                n = g;
                            }
                            release(op1);
                        }
                        break;
                    case F_LE:
                        r = g <= n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            if (r) {
                                g = todouble(op1);
                                release(op1);
                                r = n <= g;
                                n = g;
                            }
                            release(op1);
                        }
                        break;
                    case F_GT:
                        r = g > n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            if (r) {
                                g = todouble(op1);
                                release(op1);
                                r = n > g;
                                n = g;
                            }
                            release(op1);
                        }
                        break;
                    case F_GE:
                        r = g >= n;
                        while (params->pop_back(params, &op1)) {
                            assert_notblock(op1, "", func->data);
                            if (r) {
                                g = todouble(op1);
                                release(op1);
                                r = n >= g;
                                n = g;
                            }
                            release(op1);
                        }
                        break;
                }
                sprintf(buf, "%d", !!r);
                zfree(func->data);
                func->data = salloc(buf);
                s->push(s, func);
                break;
            }
            case F_EVAL: {
                if (!params->count) {
                    release(func);
                    break;
                }
                token_t* arg;
                zfree(func->data);
                while (params->pop_back(params, &arg)) {
                    arg = repr(arg, 0);
                    func->data = sadd(func->data, arg->data);
                    if (params->count)
                        func->data = sadd(func->data, " ");
                    release(arg);
                }
                sourcedata_t* seval = evalsrc(func->data, ln, salloc(sn));
                sourcedata_t* os = current;
                func->data = NULL;
                release(func);
                current = seval;

                phrase_t* p = NULL;
                vec_token_t* objs = NULL;
                while ((p = tokenize())) {
                    if (!p) {
                        release(seval);
                        current = os;
                        break;
                    }
                    objs = parse_phrase(p, cs);
                    release(p);
                    if (!objs)
                        break;
                    while (!sq && objs && eval_expressions(objs, args) && !sq && eval_statements(objs, args) &&
                        !(flags & EV_QUITCALLED) && !sq && exec(objs));
                    while (objs->pop_back(objs, &func))
                        s->push(s, func);
                    release(objs);
                }
                flags &= ~EV_RETURN;
                release(seval);
                current = os;
                break;
            }
            case F_BUILTIN: {
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                release(func);
                f = fid(arg->data);
                if (!f)
                    err(sn, ln, "error, '%s' is not a builtin function!\n", arg->data);
                func = arg;
                goto callbuiltin;
            }
            case F_BLOCK: {
                token_t* arg;
                token_t* block;
                token_t* t;
                block = alloc(token_t);
                block->type = T_BLOCK;
                block->block = alloc(block_t);
                t = alloc(token_t);
                t->lineno = func->lineno;
                t->source = func->source;
                t->type = T_CODE;
                t->data = salloc("{");
                block->block->data->push(block->block->data, t);
                while (params->pop_back(params, &arg)) {
                    if (arg->type != T_BLOCK) {
                        if (eq(arg->data, "(") && arg->type == T_CMD)
                            arg->type = T_EXPR;
                        if (eq(arg->data, ")") && arg->type == T_CMD)
                            arg->type = T_ENDEXPR;
                        block->block->data->push(block->block->data, arg);
                        ln = arg->lineno;
                        arg = NULL;
                    } else {
                        while (arg->block->data->pop_back(arg->block->data, &t)) {
                            block->block->data->push(block->block->data, t);
                            ln = t->lineno;
                        }
                    }
                    release(arg);
                }
                t = alloc(token_t);
                t->lineno = ln;
                t->source = func->source;
                t->type = T_ENDCODE;
                t->data = salloc("}");
                block->block->data->push(block->block->data, t);
                v->push_back(v, block);
                release(func);
                break;
            }
            case F_FLAT: {
                token_t* arg;
                release(func);
                while (params->pop_back(params, &arg)) {
                    if (arg->type == T_BLOCK) {
                        decap_block(arg->block->data);
                        for (size_t i=0; i<arg->block->data->count; ++i) {
                            arg->block->data->get(arg->block->data, i, &t);
                            if (t->type == T_EXPR || t->type == T_ENDEXPR)
                                t->type = T_CMD;
                        }
                        vec_token_t* objs = parse_phrase(arg->block->data, cs);
                        release(arg);
                        each(objs, arg, s->push(s, arg););
                        release(objs);
                        continue;
                    }
                    s->push(s, arg);
                }
                break;
            }
            case F_EACH: {
                token_t* arg;
                params->first(params, &arg);
                if (arg->type == T_BLOCK) {
                    token_t* proc;
                    params->pop_back(params, &proc);
                    vec_token_t* fa = alloc(vec_token_t);
                    for (size_t n=0; n<params->count; ++n) {
                        token_t* pcopy;
                        pcopy = copy_token(proc);
                        params->get(params, n, &arg);
                        fa->push(fa, copy_token(arg));
                        vec_token_t* ret = interpret_block(pcopy, 0, NULL, fa);
                        flags &= ~EV_RETURN;
                        while (ret && ret->pop_back(ret, &pcopy))
                            s->push(s, pcopy);
                        fa->pop(fa, &arg);
                        release(arg);
                        release(ret);
                    }
                    release(fa);
                    release(proc);
                    release(func);
                    break;
                }
                params->pop_back(params, &arg);
                if (arg->type == T_CMD) {
                    token_t* r = getvar(arg, args);
                    if (r && r->type == T_BLOCK) {
                        while (params->pop_back(params, &a)) {
                            vec_token_t* fa = alloc(vec_token_t);
                            fa->push(fa, a);
                            vec_token_t* ret = interpret_block(r, 0, NULL, fa);
                            flags &= ~EV_RETURN;
                            while (ret && ret->pop_back(ret, &r))
                                s->push(s, r);
                            release(a);
                            release(fa);
                            release(ret);
                            r = getvar(arg, args);
                        }
                        release(func);
                        release(arg);
                        release(r);
                        break;
                    }
                    if (r && fid(arg->data)) {
                        for (size_t n=0; n<params->count; ++n) {
                            vec_token_t* objs = alloc(vec_token_t);
                            token_t* be = alloc(token_t);
                            token_t* ee = alloc(token_t);
                            be->type = T_EXPR;
                            be->data = salloc("(");
                            be->lineno = arg->lineno;
                            be->source = arg->source;
                            ee->type = T_ENDEXPR;
                            ee->data = salloc(")");
                            ee->lineno = arg->lineno;
                            ee->source = arg->source;
                            params->get(params, n, &a);
                            objs->push(objs, copy_token(arg));
                            objs->push(objs, be);
                            objs->push(objs, copy_token(a));
                            objs->push(objs, ee);
                            showtokens(objs);
                            vec_token_t* sobjs = parse_phrase(objs, cs);
                            release(objs);
                            eval_expressions(sobjs, NULL);
                            token_t* o;
                            while (sobjs && sobjs->pop_back(sobjs, &o))
                                s->push(s, o);
                            release_phrase(sobjs);
                            release(r);
                        }
                        release(arg);
                        release(func);
                        release(r);
                        break;
                    }
                    release(r);
                }
                release(func);
                err(sn, ln, "error, 'each' first argument must be a function!\n");
            }
            case F_READ: {
                size_t n = 0;
                token_t* arg;
                if (!params->count) {
                    zfree(func->data);
                    if (getline(&func->data, &n, stdin)>0) {
                        trim(func->data);
                        func->type = T_QUOTE;
                        s->push(s, func);
                    } else {
                        release(func);
                    }
                    break;
                }
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                FILE* fl = fopen(arg->data, "r");
                if (!fl)
                    err(func->source, func->lineno, "error, unable to read '%s'.\n", arg->data);
                char* tmp = NULL;
                zfree(func->data);
                while (getline(&tmp, &n, fl)>0) {
                    func->data = sadd(func->data, tmp);
                    zfree(tmp);
                }
                func->type = T_QUOTE;
                s->push(s, func);
                free(tmp);
                fclose(fl);
                release(arg);
                break;
            }
            case F_WRITE:
            case F_APPEND: {
                token_t* op1;
                token_t* op2;
                params->pop_back(params, &op1);
                assert_notblock(op1, "first", func->data);
                params->pop_back(params, &op2);
                assert_notblock(op2, "second", func->data);

                zfree(func->data);
                FILE* fl = NULL;
                switch (f) {
                    case F_WRITE: fl = fopen(op2->data, "w+"); break;
                    case F_APPEND: fl = fopen(op2->data, "a"); break;
                }
                if (!fl)
                    err(op2->source, op2->lineno, "error, unable to write to '%s'.\n", op2->data);
                sprintf(buf, "%d", fprintf(fl, "%s", op1->data));
                func->data = salloc(buf);
                func->type = T_QUOTE;
                s->push(s, func);
                fclose(fl);
                release(op1);
                release(op2);
                break;
            }
            case F_ENV: {
                char* venv = NULL;
                token_t* arg;
                params->pop_back(params, &arg);
                assert_notblock(arg, "first", func->data);
                func->type = T_QUOTE;
                zfree(func->data);
                venv = getenv(arg->data);
                release(arg);
                func->data = salloc(venv ? venv : "");
                s->push(s, func);
                break;
            }
            case F_CWD: {
                int c = 1;
                char* ret = NULL;
                char* m = NULL;
                char* b = malloc(BUFSIZ*sizeof(char));

                while (!(ret = getcwd(b, BUFSIZ*c)) && errno == ERANGE && c < 100) {
                    ++c;
                    m = realloc(b, ((BUFSIZ*c)*sizeof(char)));
                    if (!m)
                        err(func->source, func->lineno, "error, cannot allocate enough memory to hold CWD!\n");
                    b = m;
                }
                if (!ret)
                    err(func->source, func->lineno, "error, CWD is probably too long.\n");

                zfree(func->data);
                func->type = T_QUOTE;
                func->data = b;
                s->push(s, func);
                break;
            }
            default: {
                token_t* r = getvar(func, args);
                if (r && r->type == T_BLOCK) {
                    token_t* t;
                    strline_t* sl = alloc(strline_t);
                    sl->key = salloc(func->data);
                    sl->val = func->lineno;
                    strace->push(strace, sl);
                    vec_token_t* ret = interpret_block(r, 0, func, params);
                    flags &= ~EV_RETURN;
                    strace->pop(strace, &sl);
                    zfree(sl->key);
                    release(sl);
                    release(func);
                    while (ret && ret->pop(ret, &t))
                        v->push_back(v, t);
                    release(ret);
                } else {
                    s->push(s, func);
                    func = NULL;
                    while (params->pop_back(params, &a))
                        s->push(s, a);
                    release(r);
                }
                break;
            } 
        }
        debug("------------------------\n");
        debug("STACK VALUES:\n");
        showtokens(s);
        debug("------------------------\n");
        func = NULL;
        release_phrase(params);
    }
    push_args:
    while (s->pop_back(s, &o))
        v->push(v, o);
    release(s);
    #ifdef ENABLE_X11
    if (display && todel && widgets) {
        unsigned long wid;
        while (todel->pop(todel, &wid)) {
            del(wid);
            scope_t* ws;
            if (wscopes && wscopes->get(wscopes, wid, &ws)) {
                release(ws);
                wscopes->remove(wscopes, wid);
            }
        }
    }
    #endif
    release(blocks);
    debug("RETURNED TOKENS:\n");
    showtokens(s);
    showtokens(v);
    debug("------------------------\n");
    return v->count;
}

size_t eval_statements(vec_token_t* v, vec_token_t* args) {
    showtokens(v);
    #ifdef ENABLE_X11
    widget_t* w;
    #endif
    unsigned long ln = 0;
    char* sn = NULL;
    token_t* cmd;
    token_t* ft;
    token_t* st;
    token_t* tt;
    token_t* o;

    #define consume_phrase(P) \
        while (P->pop(P, &o)) \
            release(o)

    if (!v->first(v, &ft))
        return 0;
    if (ft->type == T_CMD && v->get(v, 1, &st) && (st->type == T_DEFINE || st->type == T_DEFATTR)) {
        v->pop_back(v, &ft);
        v->pop_back(v, &st);
        tt = NULL;

        if (!v->count) {
            tt = NULL;
        } else if (v->count == 1) {
            if (v->first(v, &tt))
                v->pop_back(v, &tt);
        } else {
            token_t* t;
            tt = alloc(token_t);
            tt->block = alloc(block_t);
            tt->type = T_BLOCK;
            t = alloc(token_t);
            t->type = T_CODE;
            t->lineno = st->lineno;
            t->source = st->source;
            t->data = salloc("{");
            tt->block->data->push(tt->block->data, t);
            while (v->pop_back(v, &t)) {
                t->lineno = st->lineno;
                t->source = st->source;
                if (t->type == T_BLOCK) {
                    while (t->block->data->pop_back(t->block->data, &o)) {
                        tt->block->data->push(tt->block->data, o);
                        o->lineno = st->lineno;
                        o->source = st->source;
                    }
                    release(t);
                } else {
                    tt->block->data->push(tt->block->data, t);
                }
            }
            t = alloc(token_t);
            t->type = T_ENDCODE;
            t->lineno = st->lineno;
            t->source = st->source;
            t->data = salloc("}");
            tt->block->data->push(tt->block->data, t);
        }
        if (st->type == T_DEFATTR) {
            if (!it)
                err(ft->source, ft->lineno, "error, no implied subject!\n");
            if (aid(ft->data))
                err(ft->source, ft->lineno, "error, cannot override default attributes!\n");
            if (!wscopes)
                wscopes = alloc(map_widscope_t);
            scope_t* ws;
            if (!wscopes->get(wscopes, it->wid, &ws)) {
                ws = alloc(scope_t);
                wscopes->insert(wscopes, it->wid, ws);
            }
            define(ft, tt, ws, args);
        } else {
            define(ft, tt, NULL, args);
        }
        release(ft);
        release(st);
        release(tt);
        return 0;
    }
    while (v->count) {
        v->first(v, &cmd);
        sn = cmd->source;
        ln = cmd->lineno;
        if (cmd->type == T_BLOCK)
            err(sn, ln, "error, unused block!\n");
        if (!v->first(v, &cmd))
            return 0;
        if (cmd->type == T_QUOTE || cmd->type == T_DQUOTE)
            err(sn, ln, "error, unused string '%s'!\n", cmd->data);
        int gc = kid(cmd->data);

        if (!gc) {
            #ifdef ENABLE_X11
            if (!display)
                err(sn, ln, "error, no display and/or wrong syntax!\n");
            return v->count;
            #else
            err(sn, ln, "error, unknown command '%s'.\n", cmd->data);
            #endif
        }
        v->pop_back(v, &cmd);
        long nargs = cargs(gc);
        if (gc && ((!nargs && v->count) ||
            (nargs > 0 && ((long)v->count < nargs)) ||
            (nargs < -1 && ((long)v->count < (labs(nargs)-1)))))
        {
            err(sn, ln, "error, wrong number of arguments for command '%s'.\n", cmd->data);
        }
        switch (gc) {
        case CMD_QUIT:
            flags |= EV_QUITCALLED;
            exit_status = 0;
            consume_phrase(v);
            break;
        case CMD_EXIT:
            v->pop_back(v, &ft);
            assert_notblock(ft, "first", cmd->data);
            flags |= EV_QUITCALLED;
            exit_status = tolong(ft);
            release(ft);
            consume_phrase(v);
            break;
        case CMD_CD:
            v->pop_back(v, &ft);
            assert_notblock(ft, "first", cmd->data);

            if (chdir(ft->data) < 0) {
                perror("chdir");
                err(ft->source, ft->lineno, "error, unable to change working dir.\n");
            }
            release(ft);
            break;
        case CMD_VARS: {
            strtoken_t* t;
            scope_t* sc = cs;

            #ifdef ENABLE_X11
            if (v->first(v, &ft)) {
                v->pop_back(v, &ft);
                assert_notblock(ft, "first", cmd->data);

                if (!display)
                    err(sn, ln, "error, no display!\n");

                unsigned long wid = strtoul(ft->data, NULL, 10);
                release(ft);
                if (!wscopes || !wid || !wscopes->get(wscopes, wid, &sc))
                    break;
            }
            #endif

            eachitem(sc->memos, strtoken_t, t,
                switch (t->val->type) {
                case T_BLOCK: {
                    token_t* r = repr(copy_token(t->val), 0);
                    fprintf(stderr, "%s[<block>]: %s\n", t->key, r->data);
                    release(r);
                    break;
                }
                default:
                    fprintf(stderr, "%s[<var>]: %s\n", t->key, t->val->data);
                    break;
                }
            );
            break;
        }
        case CMD_SOURCE:
            v->pop_back(v, &ft);
            assert_notblock(ft, "first", cmd->data);
            new_source(S_SOURCE, ft->data);
            release(ft);
            release(cmd);
            break;
        case CMD_RUN:
            v->pop_back(v, &ft);
            ft = repr(ft, 1);
            int ret = system(ft->data);
            ++ret;
            release(ft);
            break;
        case CMD_PUTS:
            while (v->pop_back(v, &ft)) {
                ft = repr(ft, 0);
                printf("%s", ft->data);
                release(ft);
                if (v->count)
                    printf(" ");
            }
            printf("\n");
            break;
        case CMD_PRINT:
        case CMD_ERROR:
            while (v->pop_back(v, &ft)) {
                ft = repr(ft, 0);
                fprintf(gc == CMD_PRINT ? stdout : stderr, "%s", ft->data);
                release(ft);
                if (v->count)
                    printf(" ");
            }
            break;
        case CMD_UNSET: {
            v->pop_back(v, &ft);
            assert_notblock(ft, "first", cmd->data);

            #ifdef ENABLE_X11
            if (v->first(v, &st)) {
                v->pop_back(v, &st);
                if (!display)
                    err(sn, ln, "error, no display!\n");

                assert_notblock(st, "second", cmd->data);

                char* k = ft->data;
                token_t* v;
                strtoken_t* i;
                scope_t* ws;
                unsigned long wid = strtoul(ft->data, NULL, 10);
                if (wscopes && wid && wscopes->get(wscopes, wid, &ws) && ws->memos->item(ws->memos, st->data, &i)) {
                    k = i->key;
                    v = i->val;
                    ws->memos->remove(ws->memos, st->data);
                    zfree(k);
                    release(v);
                }
                release(ft);
                release(st);
                break;
            }
            #endif

            int x;
            timetoken_t* tc;
            char* k = ft->data;
            token_t* v;
            strtoken_t* i;

            if (cs->memos->item(cs->memos, ft->data, &i)) {
                k = i->key;
                v = i->val;
                cs->memos->remove(cs->memos, ft->data);
                zfree(k);
                release(v);
            }
            x = atoi(ft->data);

            if (schedules && schedules->get(schedules, x, &tc)) {
                release(tc->val);
                release(tc);
                schedules->remove(schedules, x);
            }
            if (onetimes && onetimes->get(onetimes, x, &tc)) {
                release(tc->val);
                release(tc);
                onetimes->remove(onetimes, x);
            }
            release(ft);
            break;
        }
        case CMD_EVERY:
        case CMD_AFTER: {
            map_int_timetoken_t* m;
            if (gc == CMD_EVERY) {
                if (!schedules)
                    schedules = alloc(map_int_timetoken_t);
                m = schedules;
            } else {
                if (!onetimes)
                    onetimes = alloc(map_int_timetoken_t);
                m = onetimes;
            }
            int secs;
            timetoken_t* tc;

            v->pop_back(v, &ft);
            v->pop_back(v, &st);

            assert_notblock(ft, "first", cmd->data);
            assert_block(st, "second", cmd->data);
            secs = atoi(ft->data);

            if (m->get(m, secs, &tc)) {
                release(tc->val);
                release(tc);
                m->remove(m, secs);
            }
            release(ft);

            tc = alloc(timetoken_t);
            tc->key = time(NULL);
            tc->val = st;
            if (m->full(m))
                m->rehash(m);
            m->insert(m, secs, tc);
            break;
        }
        case CMD_IF:
        case CMD_UNLESS: {
            v->pop_back(v, &ft);
            v->first(v, &st);
            assert_block(st, "second", cmd->data);

            conds->push(conds, gc == CMD_IF ? true(ft) : !true(ft));
            if (!conds->last(conds, &lc) || !lc) {
                conds->pop(conds, &lc);
                release(ft);
                v->pop_back(v, &st);
                release(st);
                break;
            }
            if (st->type != T_BLOCK) {
                release(ft);
                if (MUSTQUIT())
                    consume_phrase(v);
                break;
            }
            v->pop_back(v, &st);
            vec_token_t* argv = NULL;
            argv = copy_phrase(args);
            vec_token_t* ret = interpret_block(st, 1, NULL, argv);
            release_phrase(argv);
            conds->pop(conds, &lc);
            if (ret) {
                release(ft);
                consume_phrase(v);
                while (ret->pop_back(ret, &ft))
                    v->push_back(v, ft);
                release(ret);
                release(cmd);
                return 0;
            }
            release(ft);
            if (MUSTQUIT())
                consume_phrase(v);
            break;
        }
        case CMD_ELSE: {
            v->first(v, &ft);

            assert_block(ft, "first", cmd->data);
            if (lc) {
                v->pop_back(v, &ft);
                release(ft);
                break;
            }
            vec_token_t* argv = NULL;
            if (ft->type != T_BLOCK) {
                if (MUSTQUIT())
                    consume_phrase(v);
                break;
            }
            v->pop_back(v, &ft);
            argv = copy_phrase(args);
            vec_token_t* ret = interpret_block(ft, 1, NULL, argv);
            release_phrase(argv);
            if (ret) {
                consume_phrase(v);
                while (ret->pop_back(ret, &ft))
                    v->push_back(v, ft);
                release_phrase(ret);
                release(cmd);
                return 0;
            }
            if (MUSTQUIT())
                consume_phrase(v);
            break;
        }
        case CMD_RETURN:
            flags |= EV_RETURN;
            showtokens(args);
            release(cmd);
            return 0;
        case CMD_WHILE:
        case CMD_UNTIL: {
            int forever = 0;
            token_t* cond;
            token_t* block;
            v->pop_back(v, &cond);
            v->pop_back(v, &block);
            if (cond->type != T_BLOCK) {
                int istrue = true(cond);
                if ((!istrue && gc == CMD_WHILE) || (istrue && gc == CMD_UNTIL)) {
                    release(cond);
                    release(block);
                    break;
                } else {
                    forever = 1;
                }
            } else {
                decap_block(cond->block->data);
            }
            assert_block(block, "body", cmd->data);
            token_t* o = NULL;
            while (!MUSTQUIT() && !EXITLOOP()) {
                if (!forever) {
                    token_t* evaluated_cond = copy_token(cond);
                    o = NULL;
                    vec_token_t* objs = parse_phrase(evaluated_cond->block->data, cs);
                    release(evaluated_cond);
                    size_t res = eval_expressions(objs, NULL);
                    int istrue = 1;
                    while (objs->pop_back(objs, &o)) {
                        if (!true(o)) {
                            release(o);
                            istrue = 0;
                            break;
                        }
                        release(o);
                    }
                    release_phrase(objs);
                    if ((gc == CMD_WHILE && (!res || !istrue)) || ((gc == CMD_UNTIL && (res && istrue))))
                        break;
                }
                vec_token_t* argv = NULL;
                argv = copy_phrase(args);
                vec_token_t* ret = interpret_block(copy_token(block), 1, NULL, argv);
                release_phrase(argv);
                if (ret) {
                    consume_phrase(v);
                    while (ret->pop_back(ret, &o))
                        v->push_back(v, o);
                    release_phrase(ret);
                    flags &= ~EV_BREAK;
                    release(block);
                    release(cond);
                    release(cmd);
                    return 0;
                }
                #ifdef ENABLE_X11
                if (display)
                    process_events();
                #endif
                schedule();
            }
            flags &= ~EV_BREAK;
            release(block);
            release(cond);
            if (MUSTQUIT())
                consume_phrase(v);
            break;
        }
        case CMD_FOR: {
            token_t* o;
            token_t* block = NULL;
            token_t* evaluated_block = NULL;
            vec_token_t* vars = alloc(vec_token_t);
            vec_token_t* vs = alloc(vec_token_t);

            while (v->first(v, &o) && !(o->type == T_CMD && eq(o->data, "in"))) {
                v->pop_back(v, &o);
                vars->push(vars, o);
            }
            if (!v->pop_back(v, &o))
                err(sn, ln, "error, wrong syntax for 'for' command!\n");
            release(o);
            while (v->first(v, &o) && !(o->type == T_CMD && *o->data == ':')) {
                v->pop_back(v, &o);
                vs->push(vs, o);
            }
            if (!v->pop_back(v, &o))
                err(sn, ln, "error, wrong syntax for 'for' command!\n");
            release(o);
            if (!v->pop_back(v, &block) || (block->type != T_BLOCK))
                err(sn, ln, "error, no code block given to 'for' command!\n");

            size_t times = (vs->count / vars->count) + (vs->count % vars->count);
            size_t z = 0;
            for (size_t i=0; (!MUSTQUIT() && !EXITLOOP() && i<times); ++i) {
                evaluated_block = copy_token(block);
                release(evaluated_block->block->env);
                scope_t* es = alloc(scope_t);
                evaluated_block->block->env = es;
                for (size_t j=0; j<vars->count; ++j) {
                    char* k;
                    token_t* var;
                    token_t* val;

                    vars->get(vars, j, &var);
                    if (vs->get(vs, z++, &val)) {
                        k = salloc(var->data);

                        if (es->memos->full(es->memos))
                            es->memos->rehash(es->memos);
                        es->memos->insert(es->memos, k, copy_token(val));
                    }
                }
                vec_token_t* argv = NULL;
                argv = copy_phrase(args);
                vec_token_t* ret = interpret_block(evaluated_block, 0, NULL, argv);
                release_phrase(argv);
                if (ret) {
                    consume_phrase(v);
                    while (ret->pop_back(ret, &o))
                        v->push_back(v, o);
                    release_phrase(ret);
                    flags &= ~EV_BREAK;
                    release(block);
                    release_phrase(vars);
                    release_phrase(vs);
                    release(cmd);
                    return 0;
                }
                #ifdef ENABLE_X11
                if (display)
                    process_events();
                #endif
                schedule();
            }
            flags &= ~EV_BREAK;
            release(block);
            release_phrase(vars);
            release_phrase(vs);
            if (MUSTQUIT())
                consume_phrase(v);
            break;
        }
        case CMD_BREAK:
            flags |= EV_BREAK;
            break;
        case CMD_PASS:
            consume_phrase(v);
            break;
        case CMD_SUBCMD: {
            sig s;

            showtokens(v);
            v->pop_back(v, &ft);
            v->pop_back(v, &st);

            assert_notblock(ft, "first", cmd->data);
            assert_block(st, "second", cmd->data);

            s = gsid(ft->data);
            if (s) {
                switch (s) {
                    case SIG_TERM: {
                        struct sigaction act;
                        memset(&act, 0, sizeof(act));
                        act.sa_handler = term_handler;
                        sigaction(SIGINT, &act, NULL);
                        sigaction(SIGTERM, &act, NULL);
                    }
                    default:
                        break;
                }
                register_action(s, st);
                release(ft);
                break;
            }
            #ifdef ENABLE_X11
            if (!display)
                err(sn, ln, "error, no display!\n");
            if (!it)
                err(sn, ln, "error, no implied subject.\n");
            s = sidbe(it->etype, ft->data);

            if (!s)
                err(ft->source, ft->lineno, "error, unknown signal '%s'.\n", ft->data);
            switch (s) {
                case SIG_PRESSED:
                case SIG_LPRESSED:
                case SIG_RPRESSED:
                case SIG_MPRESSED:
                case SIG_SCROLLUP:
                case SIG_SCROLLDOWN:
                    it->mask |= ButtonPressMask;
                    break;
                case SIG_RELEASED:
                case SIG_LRELEASED:
                case SIG_RRELEASED:
                case SIG_MRELEASED:
                    it->mask |= ButtonReleaseMask;
                    break;
                case SIG_CHECKED:
                case SIG_UNCHECKED:
                case SIG_CLICKED:
                case SIG_DCLICKED:
                case SIG_LCLICKED:
                case SIG_RCLICKED:
                case SIG_MCLICKED:
                case SIG_LDCLICKED:
                case SIG_RDCLICKED:
                case SIG_MDCLICKED:
                    it->mask |= ButtonPressMask|ButtonReleaseMask;
                    break;
                case SIG_ENTER:
                    it->mask |= EnterWindowMask;
                    break;
                case SIG_LEAVE:
                    it->mask |= LeaveWindowMask;
                    break;
                case SIG_FOCUS:
                case SIG_UNFOCUS:
                    it->mask |= FocusChangeMask;
                    break;
                case SIG_RETURNPRESSED:
                    it->mask |= KeyPressMask|KeyReleaseMask;
                    break;
                default:
                    break;
            }
            XSelectInput(display, it->wid, it->mask);
            register_widaction(it->wid, s, st);
            release(ft);
            break;
            #else
            else {
                err(ft->source, ft->lineno, "error, unknown signal '%s'.\n", ft->data);
            }
            #endif
            release(ft);
            release(st);
            break;
        }
        case CMD_DELSUBCMD: {
            v->pop_back(v, &ft);
            assert_notblock(ft, "first", cmd->data);
            unregister(ft);
            release(ft);
            break;
        }
        case CMD_WAIT: {
            v->pop_back(v, &ft);

            assert_notblock(ft, "first", cmd->data);
            double secs = todouble(ft);
            if (secs) {
                secs *= 1000000;
                struct timeval tw;
                struct timeval now;

                memset(&tw, 0, sizeof(tw));
                gettimeofday(&tw, NULL);
                long elapsed = 0;
                do {
                    #ifdef ENABLE_X11
                    if (display && widgets && widgets->count) {
                        poll_and_process_events(secs > 100000 ? 100000 : secs);
                    } else {
                    #else
                    {
                    #endif
                        usleep(secs);
                    }
                    schedule();
                    memset(&now, 0, sizeof(now));
                    gettimeofday(&now, NULL);
                    elapsed = ((now.tv_sec * 1000000) + now.tv_usec) - ((tw.tv_sec * 1000000) + tw.tv_usec);
                } while (elapsed <= secs);
            }
            release(ft);
            break;
        }
        #ifdef ENABLE_X11
        case CMD_DELETE: {
            if (!display)
                err(sn, ln, "error, no display!\n");
            v->pop_back(v, &ft);
            assert_notblock(ft, "first", cmd->data);
            Window eid = strtoul(ft->data, 0, 10);

            if (eid && widgets->get(widgets, eid, &w)) {
                if (w == it)
                    it = NULL;
                hide(w);
                w->flags |= F_GARBAGE;
                todel->push(todel, w->wid);
            } else {
                warn(ft->source, ft->lineno, "warning, no window with id '%lu'.\n", eid);
                consume_phrase(v);
            }
            release(ft);
            break;
        }
        case CMD_WIDS: {
            if (!display)
                err(sn, ln, "error, no display!\n");
            widwidget_t* t;
            eachitem(widgets, widwidget_t, t, fprintf(stderr, "W: %lu C: %s\n", t->key, ename(t->val->etype)););
            break;
        }
        case CMD_RELATE: {
            if (!display)
                err(sn, ln, "error, no display!\n");

            v->pop_back(v, &ft);
            v->pop_back(v, &st);
            v->pop_back(v, &tt);
            assert_notblock(ft, "first", cmd->data);
            assert_notblock(st, "second", cmd->data);
            assert_notblock(tt, "third", cmd->data);

            unsigned long wid1 = strtoul(ft->data, NULL, 10);
            unsigned long wid2 = strtoul(st->data, NULL, 10);

            widget_t* r1;
            widget_t* r2;
            if (!wid1 || !widgets->get(widgets, wid1, &r1))
                err(ft->source, ft->lineno, "error, no widget with id '%lu'.\n", wid1);   
            else if (!wid2 || !widgets->get(widgets, wid2, &r2))
                err(st->source, st->lineno, "error, no widget with id '%lu'.\n", wid2);   
            else
                relate(r1, r2, tt->data);
            release(ft);
            release(st);
            release(tt);
            break;
        }
        #ifdef ENABLE_CONTROL
        case CMD_SENDKEYS:
        case CMD_SENDCONTROLKEYS: {
            if (!display)
                err(sn, ln, "error, no display!\n");

            v->pop_back(v, &ft);
            v->pop_back(v, &st);

            assert_notblock(ft, "first", cmd->data);
            assert_notblock(st, "second", cmd->data);
            unsigned long wid = strtoul(ft->data, NULL, 10);
            widget_t* w;
            if (wid && widgets->get(widgets, wid, &w)) {
                if (gc == CMD_SENDKEYS)
                    sendkeys(w, st->data);
                else
                    sendcontrolkeys(w, st->data);
                release(ft);
                release(st);
            } else {
                err(ft->source, ft->lineno, "error, no widget with id '%lu'.\n", wid);
            }
            break;
        }
        #endif
        default:
            if (!display && v->count)
                err(sn, ln, "error, no display and/or wrong syntax!\n");
            release(cmd);
            return v->count;
        #else
        default:
            err(sn, ln, "error, unknown command '%s'.\n", cmd->data);
        #endif    
        }
        release(cmd);
        #ifdef ENABLE_X11
        if (display && todel && widgets) {
            unsigned long wid;
            while (todel->pop(todel, &wid)) {
                del(wid);
                scope_t* ws;
                if (wscopes && wscopes->get(wscopes, wid, &ws)) {
                    release(ws);
                    wscopes->remove(wscopes, wid);
                }
            }
        }
        #endif
    }
    showtokens(v);
    if (!display && v->count)
        err(sn, ln, "error, no display and/or wrong syntax!\n");
    return v->count;
}

void evaluator_free() {
    strline_t* sl;
    each(strace, sl,
        free(sl->key);
        release(sl);
    );
    release(strace);

    int_timetoken_t* itc;
    sigtoken_t* sc;

    #ifdef ENABLE_X11
    widwidget_t* ww = NULL;
    wid_sigcodes_t* wacts = NULL;
    widscope_t* ws = NULL;

    if (widgets) {
        eachitem(widgets, widwidget_t, ww, del(ww->key););
        release(widgets);
    }
    if (widacts) {
        eachitem(widacts, wid_sigcodes_t, wacts,
            each(wacts->val, sc,
                if (sc)
                    release(sc->val);
                release(sc);
            );
            release(wacts->val);
        );
        release(widacts);
    }
    if (wscopes) {
        eachitem(wscopes, widscope_t, ws, release(ws->val););
        release(wscopes);
    }
    release(todel);
    if (widlines) {
        widloci_t* l;
        eachitem(widlines, widloci_t, l,
            release(l->val);
        );
        release(widlines);
    }
    if (widareas) {
        widloci_t* l;
        eachitem(widareas, widloci_t, l,
            release(l->val);
        );
        release(widareas);
    }
    if (widarcssets) {
        widarcssets_t* l;
        eachitem(widarcssets, widarcssets_t, l,
            release(l->val);
        );
        release(widarcssets);
    }
    if (widarcsareas) {
        widarcssets_t* l;
        eachitem(widarcsareas, widarcssets_t, l,
            release(l->val);
        );
        release(widarcsareas);
    }
    if (widloci) {
        widloci_t* l;
        eachitem(widloci, widloci_t, l,
            if (l)
                release(l->val);
        );
        release(widloci);
    }
    if (widpixels) {
        widloci_t* l;
        eachitem(widpixels, widloci_t, l,
            if (l)
                release(l->val);
        );
        release(widpixels);
    }
    if (widnamedpoints) {
        widnamedpoints_t* l;
        eachitem(widnamedpoints, widnamedpoints_t, l,
            if (l)
                release(l->val);
        );
        release(widnamedpoints);
    }
    #endif
    if (schedules) {
        eachitem(schedules, int_timetoken_t, itc,
            release(itc->val->val);
            release(itc->val);
        );
        release(schedules);
    }
    if (onetimes) {
        eachitem(onetimes, int_timetoken_t, itc,
            release(itc->val->val);
            release(itc->val);
        );
        release(onetimes);
    }
    if (actions) {
        eachitem(actions, sigtoken_t, sc,
            if (sc)
                release(sc->val);
        );
        release(actions);
    }
    each(scopes, cs, release(cs););
    release(scopes);
    release(conds);
}

void evaluator_init() {
    strace = alloc(vec_strline_t);
    scopes = alloc(scopes_t);
    conds = alloc(vec_int_t);
    cs = alloc(scope_t);
    scopes->push(scopes, cs);
    lc = 1;
}

scope_t* copy_scope(scope_t* s) {
    if (!s)
        return NULL;

    scope_t* r = alloc(scope_t);
    r->memos->strcmp = s->memos->strcmp;

    strtoken_t* m;
    eachitem(s->memos, strtoken_t, m, r->memos->insert(r->memos, salloc(m->key), copy_token(m->val)););
    return r;
}

void scope_t_free(scope_t* s) {
    strtoken_t* m;

    eachitem(s->memos, strtoken_t, m,
        free(m->key);
        release(m->val);
    );
    release(s->memos);
    free(s);
}

scope_t* scope_t_init(scope_t* s) {
    if (!s)
        return NULL;

    s->memos = alloc(map_strtoken_t);
    s->memos->strcmp = 1;
    s->free = scope_t_free;
    return s;
}

block_t* copy_block(block_t* b) {
    if (!b)
        return NULL;

    block_t* r = alloc(block_t);
    token_t* t;
    each(b->data, t, r->data->push(r->data, copy_token(t)););
    if (b->env)
        r->env = copy_scope(b->env);
    return r;
}

void block_t_free(block_t* b) {
    token_t* t;
    each(b->data, t, release(t););
    release(b->data);
    release(b->env);
    free(b);
}

block_t* block_t_init(block_t* b) {
    if (!b)
        return NULL;

    b->data = alloc(vec_token_t);
    b->env = NULL;
    b->free = block_t_free;
    return b;
}
