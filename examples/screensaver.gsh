#!/usr/bin/env guish
#######################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

op = mul
r = 1
t = |t|
    s"w:@{SW};h:@{SH}"!n+
    => c { q }

while 1 {
    r = op(@r,2)
    / A
    wait 0.1
    col = join('#', hex(mod(rand(), 256)), hex(mod(rand(), 256)), hex(mod(rand(), 256)))
    / A @col div(@t.w, 2) div(@t.h, 2) @r @r 0 360
    if gt(@r, mul(@SH,2)) {
        op = div
        / A
    }
    if eq(@r, 1) {
        op = mul
        / A
    }
}
